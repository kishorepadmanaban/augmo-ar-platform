import {sendOtp} from '../../scripts/uri'
import axios from 'axios'

export const projectActions = (project) => {
  return (dispatch, getState) => {
    //
    dispatch({type:"CREATE_POST",project});
  }
}

export const sendOTP = (phone) => {
  return (dispatch, getState) => {

    var data = {
      phone:phone
    }
    axios.post(sendOtp, data)
    .then(response => {
      if(response.data.status==='success'){
        dispatch({type:"SEND_OTP",payload:response.data.otp});
      }
    })
    .catch(error => {
      console.log(error);
    })
  }
}
    //
   //  axios.post(sendOtp, {
   //   method: 'POST',
   //   mode: 'cors',
   //   headers: {
   //     'Accept': 'application/json',
   //     'Content-Type': 'application/json',
   //   },
   //   body: JSON.stringify({
   //     phone:phone
   //   })
   //
   // }).then((response) => response.json())
   //    .then((responseJson) => {
   //      console.log(responseJson);
   //      if(responseJson.status==='success'){
   //        dispatch({type:"SEND_OTP",payload:responseJson.otp});
   //      }
   //    })
