const initState = {

}

const authReducer = (state = initState, action) =>{

  switch(action.type){
    case "SEND_OTP":{
    let name = {...state,otp:action.payload}
    return name}

    case "SAVE_USER":{
      let name = {...state,user:action.payload}
      return name}

    case "REMOVE_USER":{
      let name = {};
      return name
      }

    case "SAVE_ADDRESS":
    {
      let name = {...state,...action.payload}
      name.user.address = {...action.payload.address}
      // name.user.payment = [...name.user.payment,...action.payload.payment]
      return name
    }

    case "MANAGE_ADDRESS":
    {
      let name = {...state}
      name.user.address = {...action.payload} 
      return name
    }

    case "PERSONAL_DETAILS":
    {
      let name = {...state}
      name.user = {...name.user,...action.payload}
      return name
    }

    case "CLEAR_DATA":
    {
      let name = {}
      return name
    }

    default:{
    return state}
  }
}

export default authReducer;