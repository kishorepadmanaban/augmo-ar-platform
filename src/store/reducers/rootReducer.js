import authReducer from './authReducer'
import projectReducer from './projectReducer'
import {combineReducers} from 'redux'
import photoReducer from './photoReducer';

const rootReducer = combineReducers({
  auth:authReducer,
  project:projectReducer,
  photo:photoReducer
});


export default rootReducer;
