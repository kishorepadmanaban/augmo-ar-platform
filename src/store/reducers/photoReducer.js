const initState = {
  
}

const photoReducer = (state = initState, action) =>{
  switch(action.type){
    case "ADD_PRODUCT_CATEGORY_PHOTO":{
    let name = {...state,...action.payload}
    if(!name.photos || !name.photos[0]){
      name.photos = []
      name.photos[0] = {}
    }
    return name}

    case "ADD_PHOTO_PRICE":{
    let name = {...state,...action.payload}
    if(!name.photos || !name.photos[0]){
      name.photos = []
      name.photos[0] = {}
    }
    return name}

    case "ADD_FIRST_OBJECT":{
      let name = {...state}
      if(!name.photos || !name.photos[0]){
        name.photos = []
        name.photos[0] = {}
      }
      return name}

    case "ADD_PHOTO_IMAGE":{
      console.log(action.payload)
    let photo = {...state}
    if(!photo.photos)
    {
      photo.photos = []
      photo.photos[action.payload.index] = action.payload
    }else{
      photo.photos[action.payload.index] = action.payload
    }
    return photo
  }

    case "REMOVE_PHOTO_IMAGE":{
      let photo = {...state}
      delete photo.photos[action.payload.index]
      // delete photo.photos[action.payload.index].vuforia
      return photo}
    
    case "ADD_PHOTO_VIDEO":{
      let photo = {...state}
      photo.photos[action.payload.index].invitationVideo = action.payload.invitationVideo
      return photo}
    
    case "REMOVE_PHOTO_VIDEO":{
      let photo = {...state}
      delete photo.photos[action.payload.index].invitationVideo
      return photo}

    case "ADD_TARGET_WIDTH_PHOTO":{
      let photo = {...state}
      photo.photos[action.payload.index].targetWidth = action.payload.targetWidth
      return photo}

    case "SCALE_AND_POSITION_PHOTO":{
      let photo = {...state}
      photo.photos[action.payload.index].scale = action.payload.scale
      photo.photos[action.payload.index].position = action.payload.position
      photo.photos[action.payload.index].width = action.payload.width
      photo.photos[action.payload.index].height = action.payload.height
      return photo}

    case "RESET_POSITION_SCALE_PHOTO":
    {
      let photo = {...state}
      photo.photos[action.payload.index].position = [0,0];
      photo.photos[action.payload.index].scale = 100;
      return photo
    }

    case "DELETE_TARGET":
    {
      let photo = {...state}
      delete photo.photos[action.payload.index]
      return photo
    }
    
    case "CREATE_NEW_PROJECT":
    {
      let name = {}
      return name
    }

    case "ADD_PHOTO":
    {
      let photo = {...state}
      let index = photo.photos.length
      photo.photos[index] = {}
      return photo
    }

    case "DELETE_PHOTO":
    {
      let photo = {...state}
      photo.photos.splice(action.payload.index, 1)
      return photo
    }

    case "PUBLISHED":
    {
      let photo = {...state}
      photo.payment = {
        payment:false
      }
      photo.photos[action.payload.index].published = true;
      photo._id = action.payload._id;
      return photo
    }

    case "EDIT_PHOTO":
    {
      return action.payload
    }

    case "CLEAR_DATA":
    {
      let photo = {}
      return photo
    }

    case "CLEAR_PHOTO":
    {
      let photo = {}
      return photo
    }

    case "ADD_EMAIL":
    {
      let photo = {...state,...action.payload}
      return photo
    }



    default:{
    return state}
  }
}

export default photoReducer;
