const initState = {
  
}

const projectReducer = (state = initState, action) =>{
  switch(action.type){
    case "ADD_GROOM_BRIDE_NAME":{
    let name = {...state,...action.payload}
    name.title = action.payload.groom_name +" & "+action.payload.bride_name+" Wedding";
    return name}

    case "ADD_DATETIME_LOCATION":{
    let name = {...state,...action.payload}
    return name}

    case "ADD_INVITATION_IMAGE":{
    let name = {...state,...action.payload}
    return name}

    case "REMOVE_INVITATION_IMAGE":{
    delete state.invitationImage
    delete state.vuforia
    let name = {...state}
    return name}

    case "ADD_INVITATION_VIDEO":{
    let name = {...state,...action.payload}
    return name}

    case "REMOVE_INVITATION_VIDEO":{
    delete state.invitationVideo
    let name = {...state};
    return name}

    case "ADD_GALLERY_IMAGES":{

    if(state.gallery_images === undefined)
    {
      state.gallery_images = []
    }

    let images = [...state.gallery_images,...action.payload.gallery_images]
    images = {
      gallery_images:images
    }
    let name = {...state,...images}
    return name}

    case "REMOVE_GALLERY_IMAGE":{
    let name = state.gallery_images.filter((image)=>image!==action.payload)
    name = {...state,gallery_images:name}
    return name}

    case "ADD_GALLERY_VIDEO":{
    let name = {...state,...action.payload}
    return name}

    case "ADD_TEMPLATE":{
    let name = {...state,...action.payload}
    return name}

    case "SCALE_AND_POSITION":{
    let name = {...state,...action.payload}
    return name}

    case "ADD_PRODUCT_CATEGORY":
    {
      let name = {...state,...action.payload}
      return name
    }

    case "ADD_EVENT_CATEGORY":
    {
      let name = {...state,...action.payload}
      return name
    }

    case "ADD_INVITATION_PRICE":
    {
      let name = {...state,...action.payload}
      return name
    }



    case "EDIT_INVITATION":
    {
      // let name = {...state,...action.payload}
      return action.payload
    }

    case "CREATE_NEW_PROJECT":
    {
      let name = {}
      return name
    }

    case "ADD_TARGET_WIDTH":
    {
      let name = {...state,...action.payload}
      return name
    }

    case "RESET_POSITION_SCALE":
    {
      let name = {...state}
      name.position = [0,0];
      name.scale = 100;
      return name
    }

    case "CLEAR_DATA":
    {
      let name = {}
      return name
    }

    case "CLEAR_INVITATION":
    {
      let name = {}
      return name
    }

    case "INVITATION_PUBLISHED":
    {
      let name = {...state}
      name.published = true;
      return name
    }

    case "EDIT_PUBLISHED_INVITATION":
    {
      let name = {...state}
      name.published = false;
      return name
    }


    default:{
    return state}
  }
}

export default projectReducer;
