import React from 'react'
import {connect} from 'react-redux'
import {addInvitationImage, addInvitationVideo, removeInvitationImage, removeInvitationVideo} from '../scripts/uri'
import axios from 'axios'
import StarRatings from 'react-star-ratings';
import Tick from '../assets/ui/tick.svg'
import Cloud from '../assets/ui/Cloud.svg'
import ProcessStepper from './processStepper';
import Loading from './loading';

class InvitationUpload extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      upload: "",
      progress: 0,
      targetWidth: this.props.project.targetWidth?this.props.project.targetWidth:null,
      range:this.props.project.scale?this.props.project.scale:100,
      value:5,
      position:this.props.project.position?this.props.project.position:[0,0],
      invitationWidth:null,
      invitationHeight:null
    }
  }

  componentDidMount(){

    if(this.props.project.invitationVideo && this.props.project.position){
      var dragItem = document.querySelector("#item");
      dragItem.style.transform = "translate3d(" + this.props.project.position[0] + "px, " + (-this.props.project.position[1]) + "px, 0)";
    }
      this.dragElement();
  }


  slider = () => {
    var slider = document.getElementById("test5");
    this.setState({range:slider.value})
  }



  dragElement = () => {
    var dragItem = document.querySelector("#item");
    var container = document.querySelector("#container");
    var position = document.querySelector("#position");
    var invitationImage = document.querySelector("#invitationImage");
    if(container)
    {

    var active = false;
    var currentX;
    var currentY;
    var initialX;
    var initialY;
    var xOffset = this.props.project.position?this.props.project.position[0]:0;
    var yOffset = this.props.project.position?-this.props.project.position[1]:0;

    container.addEventListener("touchstart", dragStart, false);
    container.addEventListener("touchend", dragEnd, false);
    container.addEventListener("touchmove", drag, false);
  
    container.addEventListener("mousedown", dragStart, false);
    container.addEventListener("mouseup", dragEnd, false);
    container.addEventListener("mousemove", drag, false);
    container.addEventListener("mouseout", dragEnd, false);

    invitationImage.addEventListener("touchstart", dragStart, false);
    invitationImage.addEventListener("touchend", dragEnd, false);
    invitationImage.addEventListener("touchmove", drag, false);
  
    invitationImage.addEventListener("mousedown", dragStart, false);
    invitationImage.addEventListener("mouseup", dragEnd, false);
    invitationImage.addEventListener("mousemove", drag, false);
  
    function dragStart(e){
      if (e.type === "touchstart") {
        initialX = e.touches[0].clientX - xOffset;
        initialY = e.touches[0].clientY - yOffset;
      } else {
        initialX = e.clientX - xOffset;
        initialY = e.clientY - yOffset;
      }
  
      if (e.target === dragItem) {
        active = true;
      }
    }
  
    function dragEnd(e) {
      initialX = currentX;
      initialY = currentY;
      active = false;
    }
  
    function drag(e) {
      if (active) {
      
        e.preventDefault();
      
        if (e.type === "touchmove") {
          currentX = e.touches[0].clientX - initialX;
          currentY = e.touches[0].clientY - initialY;
        } else {
          currentX = e.clientX - initialX;
          currentY = e.clientY - initialY;
        }
  
        xOffset = currentX;
        yOffset = currentY;
  
        setTranslate(currentX, currentY, dragItem);
      }
    }
  
    var setTranslate=(xPos, yPos, el)=> {
      el.style.transform = "translate3d(" + xPos + "px, " + yPos + "px, 0)";
      position.innerHTML = xPos+","+yPos;
      this.setState({position:[xPos,-yPos]})
    }
  }
  }



  handleNext = (event) => {
    event.preventDefault();
    var img = document.getElementById("invitationImage");

    this.props.scaleAndPosition(parseInt(this.state.range,10),this.state.position,img.naturalWidth,img.naturalHeight);
    this.props.history.push('/gallery');
  }

  checkImage=(event)=>{
    event.persist();
    var reader = new FileReader();
    //Read the contents of Image File.
    reader.readAsDataURL(event.target.files[0]);
    reader.onloadend = (e) =>{
    var image = new Image();
    image.src = e.currentTarget.result;
    
    image.onload = e =>{
      //Determine the Height and Width.
      var height = e.currentTarget.width;
      var width = e.currentTarget.width;
      console.log(height,width)
      if (height < 400 && width < 400) {
          alert("Height or Width should be greater than 400px.");
          }else{
            this.handleImageUpload(event);
          }
        }
      };
  }

   handleImageUpload = (event)=> {
    if(event.target.files[0].size>2097152)
    {
      alert("File size exceeded - Please upload image less than 2MB in size.")
    } else{

    this.setState({loadingInvitation:true})
    const data = new FormData();
    data.append('invitationImage', event.target.files[0]);
    console.log(event.target.files[0])
    const config = {
      onUploadProgress: progressEvent => {
        let progress = (progressEvent.loaded * 66) / progressEvent.total
        this.setState({progress: progress});
      }
    }
    this.setState({loading:true})
    axios.post(addInvitationImage, data, config).then(response => {
      console.log(response.data);
      if(response.data.status==="success")
      {
        this.setState({loading:false})
        this.setState({progress: 100,loadingInvitation:false});
        this.props.addInvitationImage(response.data.image,response.data.response,this.props.auth.user.id);

      }else if(response.data.error==="Bad Image"){
        this.setState({loading:false})

        alert("Bad Image - Please upload some other image since this image is not suitable for augmentation")

      }else if(response.data.error==="Duplicate target exist"){
        this.setState({loading:false})

        alert("Duplicate image exist - Please upload any other image since this image is already exists in our database")

      }
    }).catch(error => {
      console.log(error);
    })
  }
}

  handleVideoUpload = (event) => {
    event.persist();
    if(event.target.files[0].size>5242880)
    {
      alert("File size exceeded - Please upload video less than 5MB in size.")
    }else{
      this.setState({loadingInvitationVideo:true})

      const data = new FormData();
      data.append('invitationVideo', event.target.files[0]);
  
      const config = {
        onUploadProgress: progressEvent => {
          let progress = (progressEvent.loaded * 83) / progressEvent.total
          this.setState({
            progress: progress
          });
        }
      }
      this.setState({loading:true})
  
      axios.post(addInvitationVideo, data, config).then(response => {
        this.setState({loading:false})
        var img = document.getElementById("invitationImage");
        this.setState({
          progress: 100,
          loadingInvitationVideo:false,
          status: response.data.status,
          targetWidth: img.clientWidth,
        });
        this.props.addInvitationVideo(response.data.url);
        this.props.addTargetWidth(img.clientWidth);
        this.dragElement();
  
      }).catch(error => {
        console.log(error);
      })
    }
  }

  removeInvitationVideo = (event,video) =>{
    let videoPath = video.replace("http://159.65.146.12/invitation/", '')
    const data = {
      image:videoPath
    }

    this.setState({loading:true})
    axios.delete(removeInvitationVideo, {data}).then(response => {
      this.setState({loading:false})
      this.setState({progress: 100,position:[0,0],scale:100});
      this.props.removeInvitationVideo();
      this.props.resetPositionAndScale();
    }).catch(error => {
      console.log(error);
    })
  }


  removeInvitationImage = (event) =>{
    this.setState({loading:true})
    axios.delete(removeInvitationImage+this.props.project.vuforia.target_record.target_id).then(response => {
      this.setState({loading:false})
      if(response.data.result_code==="Success" || response.data.result_code==="UnknownTarget")
      {
        this.props.removeInvitationImage();
      }
    }).catch(error => {
      console.log(error);
    })
  }



  render() {

    if(this.state.loading){
      document.getElementsByTagName("BODY")[0].style.overflow = "hidden";
    }else{
      document.getElementsByTagName("BODY")[0].style.overflow = "auto";
    }


    let videoWidth= (this.state.targetWidth*(this.state.range/100)).toString()+"px"
    return (<div>
      {this.state.loading?<Loading/>:null}
        <ProcessStepper pathname={2}/>
        <div className="row container">
          <div className="input-field col s12 m5">
            <h4>Upload Details</h4>
            <div>
            <div id="invitationWrap">
              <div>
                <h5>Upload Invitation</h5>
              </div>
              <div>
                {
                  this.props.project.invitationImage === undefined
                  ? <div id="invitationDiv"><input id="invitation" type="file" accept="image/jpeg" className="invitation" ref='invitation' onChange={this.checkImage}/>
                  <img id="upload" src={Cloud} alt="upload"/>
                  </div>:
                  <div id="tick">
                  <img src={Tick} alt="Next" width="25px"/>
                  </div>
                }
              </div>
            </div>
            <ul className="imageTerms">
                <li>*Only jpg format</li>
                <li>*Maximum 2MB</li>
                <li>*Min resolution 400*400</li>
              </ul>
              <div>
                {
                this.props.project.invitationImage !== undefined
                ?
                 <div id="close"><p className="">Invitation</p>
              <button onClick={(event)=>this.removeInvitationImage(event)} id="closebtn">X</button></div>:null
                }
                {
                  this.props.project.vuforia === undefined
                    ? null
                    : <div className="rating">
                        <StarRatings starEmptyColor="rgba(203, 211, 227, 0.3)" rating={this.props.project.vuforia.target_record.tracking_rating} numberOfStars={5} starDimension="20px" starRatedColor="#FF416C" starSpacing="3px"/></div>
                }
              </div>

            </div>

            {/* {
              this.props.project.invitationImage === undefined?null: */}
              <div>
                <div id="invitationVideoWrap">
                  <div>
                  <h5>Upload Invitation video</h5>
                  </div>
                  <div>
                  {
                    this.props.project.invitationVideo === undefined
                    ? <div id="invitationDiv"><input id="invitation" type="file" accept="video/mp4" className="invitation"  onChange={this.handleVideoUpload}/>
                    <img id="upload" src={Cloud} alt="upload"/>
                    </div>
                  : <div id="tick">
                  <img src={this.state.loadingInvitationVideo?Cloud:Tick} alt="Next" width="25px"/>
                  </div>
                  }
                </div>
              </div>
              <div className="imageTerms">
                <div>*Only MP4 format</div>
                <div>*Maximum 5MB</div>
              </div>
                <div>
                {
                  this.props.project.invitationVideo !== undefined ?
                  <div id="close"><p className="">Invitation video</p>
                    <button onClick={(event)=>this.removeInvitationVideo(event,this.props.project.invitationVideo)} id="closebtn">X</button></div>:null
                }
              </div>
              </div>
            {/* } */}
          </div>


          <div className="col s12 m7 Editor">
          <p className=" videoInfo">
            Hold and drag the video to position on the invite
          </p>
            <div className="white imageEditor">
              <img id="invitationImage" className="invitationImage" src={this.props.project.invitationImage} alt=""/>
              {
                this.props.project.invitationVideo === undefined
                  ? null
                  : <div id="container"><video id="item" width={videoWidth} controls="controls">
                      <source src={this.props.project.invitationVideo} type="video/mp4"/>
                      Your browser does not support the video tag.
                    </video></div>
              }
            </div>
            {
                this.props.project.invitationVideo === undefined
                  ? null
                  :<div className="col s11 sliderContainer">
                  <p className="col s3  resizeVideo">Resize Video:</p>
                    <p className="range-field col s7">
                      <input className="" type="range" id="test5" min="1" max="100" defaultValue={this.state.range} onInput={this.slider}/>
                    </p>
                  <p id="position"></p>
                  </div>
            }
            
          </div>
          {
          this.props.project.invitationImage !== undefined&&this.props.project.invitationVideo !== undefined?
          // <div onClick={this.handleNext} id="nextBtnInvitation">
          //     <img src={NextBtn} alt="Next" width="60px"/>
          //   </div>
          <div onClick={this.handleNext} id="nextBtnNew">
          <b>Next :</b> Upload Gallery Images
          </div>
            :null
          }
        </div>
    </div>)
  }
}

const mapStateToProps = (state) => {
  return {
    project: state.project,
    auth:state.auth
  }
}

const mapDispathToProps = (dispatch) => {
  return {
    addInvitationImage: (invitationImage,vuforia,email) => {
      dispatch({
        type: "ADD_INVITATION_IMAGE",
        payload: {
          invitationImage,
          vuforia,
          email
        }
      })
    },
    addInvitationVideo: (invitationVideo) => {
      dispatch({
        type: "ADD_INVITATION_VIDEO",
        payload: {
          invitationVideo
        }
      })
    },
    removeInvitationVideo: () => {
      dispatch({
        type: "REMOVE_INVITATION_VIDEO",
      })
    },
    addTargetWidth:(targetWidth)=>{
      dispatch({
        type: "ADD_TARGET_WIDTH",
        payload:{
          targetWidth
        }
      })
    },
    removeInvitationImage: () => {
      dispatch({
        type: "REMOVE_INVITATION_IMAGE",
      })
    },
    scaleAndPosition: (scale,position,width,height) => {
      dispatch({
        type: "SCALE_AND_POSITION",
        payload:{
          scale,
          position,
          width,
          height
        }
      })
    },
    resetPositionAndScale:()=>{
      dispatch({
        type: "RESET_POSITION_SCALE",
      })
    }
  }
}

export default connect(mapStateToProps, mapDispathToProps)(InvitationUpload)
