import React from 'react'
import {connect} from 'react-redux'
import Plus from '../assets/ui/plus.svg'
import Minus from '../assets/ui/minus.svg'
import Info from '../assets/ui/info.svg'
import { buyInfoCard } from '../scripts/uri';
import axios from 'axios'
const Razorpay = window.Razorpay

class BuyInfoCard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
        count:100,
    }
  }

  handlePayment=(address,payment)=>{
    if((address.name&&address.address&&address.pincode&&address.phone)||this.state.count===0)
    {
      var options = {
        "key": "rzp_test_gvCgxv2Z9GOOe0",
        "amount": payment.totalamount*100, // 2000 paise = INR 20
        "name": "Augmo",
        "description": "Purchase Description",
        "image": "../assets/images/augmoLogo.svg",
        "handler": response =>{
          console.log(response)
            if(response.razorpay_payment_id)
            {
              payment.payment_id = response.razorpay_payment_id
              this.setState({loading:true});
              axios.post(buyInfoCard,{address,payment}).then(res=>{
                if(res.data.status==="success")
                {
                  this.setState({loading:false});
                  alert("Info cards ordered sucessfully")
                }else{
                  this.setState({loading:false});
                  alert("Info cards ordered sucessfully")
                }
              })
            }else{
              alert("Payment failed if money debited from your account it will be refunded in 7 business days")
            }
        },
        "prefill": {
            "name": address.name,
            "email": this.props.auth.user.id
        },
        "notes": {
            "address": "Hello World"
        },
        "theme": {
            "color": "#FF416C"
        }
      };
      var rzp1 = new Razorpay(options);
      rzp1.open();
    }else{
      alert("Please fill the required fields")
    }
  }


  render () {
    let totalamount = this.state.count*2;

    let gst = totalamount-(totalamount*(100/(100+18)));
          gst = gst.toFixed(2)
    return(

        <div className="col s12 m12 background">
        <h5 id="title">Order Summary</h5>
        <div className="orderContainer">

        <div className="col s12 m12 " id="orderWrap">
        <div className="col s6 " id="orderText">
            <div className="text_right">Invitation Info Card<sup><img src={Info} alt="help" width="20px"/></sup></div>
        </div>
        <div className="col s6" id="orderPrice">
            <div>Rs.{totalamount}</div>
        </div>

        </div>
        <div className="col s6 " id="infoCardCounterWrap">
            <div id="minus" onClick={()=>this.setState({count:Math.max(100,this.state.count-100)})}><img src={Minus} alt="help" width="20px"/></div>
            <div id="count">{this.state.count}</div>
            <div id="add" onClick={()=>this.setState({count:this.state.count+100})}><img src={Plus} alt="help" width="20px"/></div>
        </div>
        <div className="col s12 m12"  id="line">
        </div>
        <div className="col s12 m12 padding10 margin0">
            <div className="flexSpaceBetween col s12 m12  padding0 margin0">
            <div className="text_right col s6 m6 padding0 margin0">CGST</div>
            <div className="text_right col s6 m6 padding0 margin0">Rs.{(gst/2).toFixed(2)}</div>
            </div>
            <div className="flexRow col s12 m12  padding0 margin0">
            <div className="text_right col s6 m6 padding0 margin0">SGST</div>
            <div className="text_right col s6 m6 padding0 margin0">Rs.{(gst/2).toFixed(2)}</div>
            </div>
            <div className="flexRow col s12 m12  padding0 margin0">
            <div className="h4 text_right col s6 m6 padding0 margin0">Total Amount</div>
            <div className="h4 text_right col s6 m6 padding0 margin0">Rs.{totalamount}</div>
            </div>
        </div>
        </div>
        <div onClick={()=>this.handlePayment(this.props.auth.user.address,{count:this.state.count,totalamount,gst})} id="saveAddress">
                Payment
        </div>
        </div>
)
  }
}


const mapStateToProps = (state) => {
  return {
    project: state.project,
    auth:state.auth
  }
}

const mapDispathToProps = (dispatch) => {
  return {
    addInvitationPrice: (invitationPrice) => {dispatch({type:"ADD_INVITATION_PRICE", payload:{invitationPrice}})}
  }
}


export default connect(mapStateToProps,mapDispathToProps)(BuyInfoCard)

