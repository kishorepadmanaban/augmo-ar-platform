import React from 'react'
import {connect} from 'react-redux'
import PriceBackground from '../assets/ui/PriceBackground.svg'
import ProcessStepper from './processStepper';


class invitationPrice extends React.Component {
  constructor(props) {
    super(props);
    this.state = {

    }
  }

  render () {
    return(
        <div>
        <ProcessStepper pathname={0}/>
            <h4 className="center ">Select Your Plan</h4>
            <div className="row container" id="containerCategory">
                <div id="priceCard" className="col s3 card" onClick={()=>{this.props.addInvitationPrice({plan:"basic",price:1990});this.props.history.push('/name');}}>
                    <img id="priceBackground" src={PriceBackground} alt="price"/>
                    <div id="discount"><div id="discountPrice">50%</div><div id="discountOff">offer</div></div>
                    <div id="priceContent">
                        <h5 className="center white-text plan">BASIC</h5>
                        <div className="center white-text mrp"><sup id="mrpsup">₹</sup>3000</div>
                        <h3 className="center white-text price"><sup id="inr">₹</sup>1990</h3>
                        <h5 className="center white-text planTitle"><sup>____</sup> PLAN <sup>____</sup></h5>
                        <p className="center white-text features">Upto 10 Gallery Images</p>
                        <p className="center white-text features">Validity 3 months*</p>
                        <p className="center white-text features">Save the date</p>
                        <p className="center white-text features">Location Map</p>
                        <p className="center white-text features">RSVP</p>
                    </div>
                </div>
                <div id="priceCardPremium"  className="col s3 card" onClick={()=>{this.props.addInvitationPrice({plan:"premium",price:2990});this.props.history.push('/name');}}>
                    <img id="priceBackground" src={PriceBackground} alt="price"/>
                    <div id="priceContent">
                        <h5 className="center white-text planPremium">PREMIUM</h5>
                        <div className="center white-text mrp"><sup id="mrpsup">₹</sup>5000</div>
                        <h2 className="center white-text price"><sup id="inr">₹</sup>2990</h2>
                        <h5 className="center white-text planTitle"><sup>____</sup> PLAN <sup>____</sup></h5>
                        <p className="center white-text features">Upto 20 Gallery Images</p>
                        <p className="center white-text features">Validity 6 months*</p>
                        <p className="center white-text features">Save the date</p>
                        <p className="center white-text features">Location Map</p>
                        <p className="center white-text features">RSVP</p>
                    </div>
                </div>
                <div id="priceCard"  className="col s3 card" onClick={()=>{this.props.addInvitationPrice({plan:"custom"});this.props.history.push('/name');}}>
                <img id="priceBackground" src={PriceBackground} alt="price"/>
                    <div id="priceContent">
                        <h5 className="center white-text plan">CUSTOM</h5>
                        <div className="center white-text mrp"><sup id="mrpsup"></sup></div>
                        <h4 className="center white-text customPrice">Contact Us</h4>
                        <h5 className="center white-text planTitle"><sup>____</sup> PLAN <sup>____</sup></h5>
                        <p className="center white-text features">Unlimited Gallery Images*</p>
                        <p className="center white-text features">Unlimited Validity*</p>
                        <p className="center white-text features">Dedicated Support</p>
                        <p className="center white-text features">All Premium <br/> Features</p>
                    </div>
                </div>
            </div>
        </div>
)
  }
}


const mapStateToProps = (state) => {
  return {project: state.project}
}

const mapDispathToProps = (dispatch) => {
  return {
    addInvitationPrice: (invitationPrice) => {dispatch({type:"ADD_INVITATION_PRICE", payload:{invitationPrice}})}
  }
}


export default connect(mapStateToProps,mapDispathToProps)(invitationPrice)
