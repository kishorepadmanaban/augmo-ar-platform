import React from 'react'
import {Input,Row} from 'react-materialize'
import {connect} from 'react-redux'
import { manageAddress } from '../scripts/uri';
import axios from 'axios'

class ManageAddress extends React.Component {
    constructor(props) {
    super(props);
    this.state = {
        data:this.props.auth.user?this.props.auth.user.address:null
    }
    }

    
  handleInput = (event) =>{
    this.setState({data:{...this.state.data,[event.target.id]:event.target.value}})
  }

  handleSubmit = () =>{
    let body={
      id:this.props.auth.user.id,
      address:this.state.data
    }
    axios.post(manageAddress,body).then(res=>{
      this.props.saveAddress(res.data.address)
    })
  }
  render () {
      let {data} = this.state
    return(
      <div className="flexCenterColumn padding30">
            <div className="h3 bold">Manage Address</div>
              <Row>
                <Input s={12} id="name" labelClassName="" label="Name" type="text" defaultValue={data.name} onChange={this.handleInput}/>
                <Input s={12} id="address" labelClassName="" label="Address" type="text" defaultValue={data.address} onChange={this.handleInput}/>
                <Input s={6} id="city" labelClassName="" label="City" type="text" defaultValue={data.city} onChange={this.handleInput}/>
                <Input s={6} id="state" labelClassName="" label="State" type="text" defaultValue={data.state} onChange={this.handleInput}/>
                <Input s={6} id="pincode" labelClassName="" label="Pin code" type="text" defaultValue={data.pincode} onChange={this.handleInput}/>
                <Input s={6} id="phone" labelClassName="" label="Phone" type="text" defaultValue={data.phone} onChange={this.handleInput}/>
            </Row>
            <div id="saveAddress" onClick={this.handleSubmit}>
                  Submit
            </div>
      </div>
    )
  }
}

const mapStateToProps = (state) =>{
  return {
    auth:state.auth
  }
}

const mapDispathToProps = (dispatch) => {
  return {
    saveAddress: (address) => {dispatch({type:"SAVE_ADDRESS",payload:address})}
  }
}



export default connect(mapStateToProps,mapDispathToProps)(ManageAddress)