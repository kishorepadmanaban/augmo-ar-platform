import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';

const styles = theme => ({
  root: {
    width:"50%",
    margin:"auto",
    background:"whitesmoke",
    borderRadius:"0px 0px 20px 20px"
  },
  backButton: {
    marginRight: theme.spacing.unit,
  },
  instructions: {
    marginTop: theme.spacing.unit,
    marginBottom: theme.spacing.unit,
  },
});

function getSteps() {
  return ['Choose', 'Upload','Preview', "Payment"];
}


class HorizontalLabelPositionBelowStepper extends React.Component {
    constructor(props) {
    super(props);
    this.state = {
        activeStep: 0,
    };
}

  componentDidMount(){
    //   console.log(this.props.pathname)
    //   if(this.props.pathname==="/product"||this.props.pathname==="/event"||this.props.pathname==="/price")
    //   {
    //     this.setState({activeStep:0})
    //   }else if(this.props.pathname==="/name"||this.props.pathname==="/date")
    //   {
    //     this.setState({activeStep:1})
    //   }else if(this.props.pathname==="/invitation"||this.props.pathname==="/gallery"||this.props.pathname==="/template")
    //   {
    //     this.setState({activeStep:2})
    //   }else if(this.props.pathname==="/preview"||this.props.pathname==="/payment")
    //   {
    //     this.setState({activeStep:3})
    //   }
  }

  changeRoute=(index)=>{
    let route = ["/product","/photo_editor","/preview","/payment"]
    if(index<this.props.pathname)
    {
      window.location.href = route[index]
    }
    console.log(this.props.pathname)
  }


  render() {
    const { classes } = this.props;
    const steps = getSteps();
    // const { activeStep } = this.state;
    return (
      <div className={classes.root}>
        <Stepper activeStep={this.props.pathname} alternativeLabel>
          {steps.map((label,index) => {
            return (
              // <Step onClick={()=>this.changeRoute(index)} key={index}>
              <Step key={index}>
                <StepLabel>{label}</StepLabel>
                {/* <StepLabel style={{cursor:index<this.props.pathname?"pointer":""}}>{label}</StepLabel> */}
              </Step>
            );
          })}
        </Stepper>
      </div>
    );
  }
}

HorizontalLabelPositionBelowStepper.propTypes = {
  classes: PropTypes.object,
};

export default withStyles(styles)(HorizontalLabelPositionBelowStepper);
