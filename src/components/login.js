import React from 'react'
import {connect} from 'react-redux'
import StyledFirebaseAuth from 'react-firebaseui/StyledFirebaseAuth';
import firebase from 'firebase/app';
import 'firebase/auth';
import axios from 'axios'
import {signup, login, forgetPassword, resetPassword } from '../scripts/uri';
import Loading from './loading';

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      videourl:"",
      upload:"",
      otpCheck:"",
      otp:false,
      loading:true,
      signup:false,
      forget_pass:false,
      reset_pass:false
    }
  }

  uiConfig = {
    signInFlow: 'popup',
    signInOptions: [
      firebase.auth.GoogleAuthProvider.PROVIDER_ID,
      // firebase.auth.FacebookAuthProvider.PROVIDER_ID,
      // firebase.auth.EmailAuthProvider.PROVIDER_ID
    ],
    callbacks: {
      signInSuccessWithAuthResult: () => false
    }
  };

  componentDidMount() {
    this.unregisterAuthObserver = firebase.auth().onAuthStateChanged(
        (user) => {
          if(user){
          this.setState({isSignedIn: !!user,user:user,loading:false});
          // console.log(user)
          // if(user.email!==this.props.auth.user.id)
          // {
          //   this.props.clearData();
          // }
          this.props.saveUser({id:user.email, type:user})
          }else{
          this.setState({isSignedIn: false,loading:false});
          }
        }
    );
    const urlParams = new URLSearchParams(window.location.search);
    const id = urlParams.get('id');
    const email = urlParams.get('email');
    if(id&&email)
    {
      this.setState({reset_pass:true})
    }
  }

  componentWillUnmount() {
    this.unregisterAuthObserver();
  }

  handleChange=(event)=>{
    this.setState({[event.target.id]:event.target.value})
  }


  handleNext = (event) => {
    event.preventDefault();
    this.props.history.push('/payment');
  }

  validateEmail=(email)=> {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  handleSignup=(event)=>{
    event.preventDefault();
    let body = {
      email:this.state.email,
      password:this.state.password
    }
    axios.post(signup,body).then(res=>{
      if(res.data.status==="success"){
        // this.props.clearData();
        this.props.saveUser({id:res.data.data.email, type:{email:res.data.data.email}})
        this.props.history.push('/product')
      }else if(res.data.status==="error"){
        alert("User already exist")
      }
    }).catch(error => {
      alert(error);
    })
  }

  handleLogin=(event)=>{
    event.preventDefault();
    let body = {
      email:this.state.email,
      password:this.state.password
    }
    axios.post(login,body).then(res=>{
      if(res.data.status === "success"){
        // if(res.data.email!==this.props.auth.user.id)
        // {
        //   this.props.clearData();
        // }
        this.props.saveUser({id:res.data.data.email, type:{email:res.data.data.email},address:res.data.data.address, phone:res.data.data.phone, name: res.data.data.name})
        this.props.history.push('/product')
      }else{
        alert("Username or Password incorrect")
      }
    }).catch(error => {
      alert(error);
    })
  }

  handleForgetPass=()=>{
    this.setState({loading:true})
    axios.post(forgetPassword,{email:this.state.email}).then(res=>{
      if(res.data.status==="success"){
        alert("Reset password link has been sent to your email");
        this.setState({forget_pass:false, loading:false})
      }
    }).catch(error => {
      alert(error);
    })
  }

  handleResetPass=()=>{
    if(this.state.password!==this.state.confirm_password)
    {
      alert("Passwords do not match")
    }else{
      const urlParams = new URLSearchParams(window.location.search);
      const id = urlParams.get('id');
      const email = urlParams.get('email');
      let body = {
        email,
        id,
        newpassword:this.state.password,
      }
      axios.post(resetPassword,body).then(res=>{
        console.log(res.data)
        if(res.data.status==="success")
        {
          alert("Password changed successfully")
          this.setState({reset_pass:false})
        }
      }).catch(error => {
        alert(error);
      })
    }
  }

  Signup=(event)=>{
    event.preventDefault();
    this.setState({signup:true})
  }

  Login=(event)=>{
    event.preventDefault();
    this.setState({signup:false, forget_pass:false, reset_pass: false})
  }


  render () {
    if(this.props.auth.user){
      this.props.history.push('/product')
    }
    return(
      <div className="parentContainer">
      {this.state.loading?<Loading/>:null}
          <div className="loginContainer">

          <div className="login">
            {!this.state.forget_pass&&!this.state.reset_pass?
            this.state.signup===false?

            <div className="loginWrap">
            <h5>Login</h5>

              <div className="input-field col s12">
                  <input id="email" type="email" className="validate" value={this.state.email} onChange={this.handleChange}/>
                  <label htmlFor="email">Email</label>
              </div>
              <div className="input-field col s12">
                  <input id="password" type="password" className="validate" onChange={this.handleChange}/>
                  <label htmlFor="password">Password</label>
              </div>
              <div className="input-field col s12">
                  <p className="forgetPassword" onClick={()=>this.setState({forget_pass:true})}>Forget password?</p>
              </div>
              <div className="loginBtnWrap col s12">
                  <input id="loginBtn" type="submit" value="Login" className="validate" onClick={this.handleLogin}/>
              </div>
              <div className="loginBtnWrap col s12">
                  <button id="loginBtn" type="submit" value="Signup" className="validate" onClick={this.Signup}>Signup</button>
              </div>
            </div>
            :<div className="loginWrap">
            <h5>Signup</h5>
            <div className="input-field col s12">
                <input id="email" type="email" className="validate" value={this.state.email} onChange={this.handleChange}/>
                <label htmlFor="email">Email</label>
            </div>
            <div className="input-field col s12">
                <input id="password" type="password" className="validate" onChange={this.handleChange}/>
                <label htmlFor="password">Password</label>
            </div>
            <div className="input-field col s12">
                <input id="phone" type="number" className="validate" value={this.state.phone} onChange={this.handleChange}/>
                <label htmlFor="phone">Phone</label>
            </div>
            <div className="loginBtnWrap col s12">
                <input id="loginBtn" type="submit" value="Signup" className="validate" onClick={this.handleSignup}/>
            </div>
            <div className="loginBtnWrap col s12">
                  <button id="loginBtn" type="submit" value="Login" className="validate" onClick={this.Login}>Login</button>
              </div>
          </div>:null}
          {this.state.forget_pass?
          <div className="loginWrap">
            <h5>Forgot Password</h5>
            <div className="input-field col s12">
                <input id="email" type="email" className="validate" value={this.state.email} onChange={this.handleChange}/>
                <label htmlFor="email">Email</label>
            </div>
            <div className="loginBtnWrap col s12">
                  <input id="loginBtn" type="submit" value="Send" className="validate" onClick={this.handleForgetPass}/>
            </div>
            <div className="loginBtnWrap col s12">
                <button id="loginBtn" type="submit" value="Login" className="validate" onClick={this.Login}>Login</button>
            </div>
          </div>:this.state.reset_pass?
          <div className="loginWrap">
          <h5>Reset Password</h5>
          <div className="input-field col s12">
                  <input id="password" type="password" className="validate" onChange={this.handleChange}/>
                  <label htmlFor="password">Password</label>
            </div>
            <div className="input-field col s12">
                  <input id="confirm_password" type="password" className="validate" onChange={this.handleChange}/>
                  <label htmlFor="password">Confirm Password</label>
            </div>
          <div className="loginBtnWrap col s12">
                <input id="loginBtn" type="submit" value="Save" className="validate" onClick={this.handleResetPass}/>
          </div>
          {/* <div className="loginBtnWrap col s12">
                <button id="loginBtn" type="submit" value="Login" className="validate" onClick={this.Login}>Login</button>
          </div> */}
        </div>:null}
        </div>
        {!this.state.forget_pass&&!this.state.reset_pass?
          !this.state.isSignedIn?
          <div>
            <div className="loginSocialTitle">
                <div className="hLine"></div>
                <div className="">Social</div>
                <div className="hLine"></div>
            </div>
            <div>
              <StyledFirebaseAuth uiConfig={this.uiConfig} firebaseAuth={firebase.auth()}/>
            </div>
          </div>:null:null
        }
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) =>{
  return {
    auth:state.auth
  }
}

const mapDispathToProps = (dispatch) => {
  return {
    sendOTP: (otp) => {dispatch({type:"SEND_OTP",payload:otp});},
    saveUser: (user) => {dispatch({type:"SAVE_USER",payload:user})},
    clearData: () => {dispatch({type:"CLEAR_DATA"})},
  }
}



export default connect(mapStateToProps,mapDispathToProps)(Login)
