import React from 'react'
import {connect} from 'react-redux'
import {addInvitationImage, addInvitationVideo, removeInvitationImage, removeInvitationVideo} from '../scripts/uri'
import axios from 'axios'
import StarRatings from 'react-star-ratings';
import NextBtn from '../assets/ui/Arrow with button.svg'
import Tick from '../assets/ui/tick.svg'
import Cloud from '../assets/ui/Cloud.svg'
import PhotoComponent from './photoComponent'
import { runInThisContext } from 'vm';

class PhotoEditor extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      count:1
    }
  }

  render() {
    let Components = [];
    for(let i=0;i<this.state.count;i++)
    {
      Components.push(<PhotoComponent count={i}/>)
    }
    return (<div className="photoContainer">
        {Components}
      <div className="container flexEnd">
        <div id="addBtn" onClick={()=>this.setState({count:this.state.count+1})}>Add</div>
        <div id="removeBtn" onClick={()=>this.setState({count:this.state.count-1})}>Remove</div>
      </div>
        <div className="container" onClick={this.handleNext}>
        <img src={NextBtn}  id="nextBtnInvitation" alt="Next" width="60px"/>
      </div>
    </div>)
  }
}

const mapStateToProps = (state) => {
  return {project: state.project}
}

const mapDispathToProps = (dispatch) => {
  return {
    addInvitationImage: (invitationImage,vuforia) => {
      dispatch({
        type: "ADD_INVITATION_IMAGE",
        payload: {
          invitationImage,
          vuforia
        }
      })
    },
    addInvitationVideo: (invitationVideo) => {
      dispatch({
        type: "ADD_INVITATION_VIDEO",
        payload: {
          invitationVideo
        }
      })
    },
    removeInvitationVideo: () => {
      dispatch({
        type: "REMOVE_INVITATION_VIDEO",
      })
    },
    removeInvitationImage: () => {
      dispatch({
        type: "REMOVE_INVITATION_IMAGE",
      })
    },
    scaleAndPosition: (scale,position) => {
      dispatch({
        type: "SCALE_AND_POSITION",
        payload:{
          scale,
          position
        }
      })
    }
  }
}

export default connect(mapStateToProps, mapDispathToProps)(PhotoEditor)
