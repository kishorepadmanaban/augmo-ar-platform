import React from 'react'
import {connect} from 'react-redux'
import axios from 'axios';
import { sendData, sendPaymentDetailsPhoto } from '../scripts/uri';
import ProcessStepper from './processStepperPhoto';
import Left from "../assets/ui/left-arrow.svg"
const Razorpay = window.Razorpay

class Payment extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      videourl:"",
      upload:"",
      otpCheck:"",
      otp:false,
      count:100,
      name:this.props.auth.address?this.props.auth.address.name:null,
      address:this.props.auth.address?this.props.auth.address.address:null,
      city:this.props.auth.address?this.props.auth.address.city:null,
      state:this.props.auth.address?this.props.auth.address.state:null,
      pincode:this.props.auth.address?this.props.auth.address.pincode:null,
      landmark:this.props.auth.address?this.props.auth.address.landmark:null,
      index:0,
      totalImages:[]
    }
  }

  componentDidMount(){
        let totalImages = this.props.photo.photos.filter(photo=>photo.published===true)
        this.setState({totalImages})
    }

  handleInput = (event) =>{
    this.setState({[event.target.id]:event.target.value})
  }



  handlePayment=(payment)=>{
    // var data = {auth:this.props.auth,photo:this.props.photo}
    // console.log(data);

    var options = {
      "key": "rzp_test_gvCgxv2Z9GOOe0",
      "amount": payment.totalAmount*100, // 2000 paise = INR 20
      "name": "Augmo",
      "description": "Purchase Description",
      "image": "../assets/images/augmoLogo.svg",
      "handler": response =>{
          payment.payment_id = response.razorpay_payment_id
          axios.post(sendPaymentDetailsPhoto,payment).then(response=>{
            if(response){
              this.props.history.push("my_orders")
              this.props.paymentPhoto(payment)
            }
          })
      },
      "prefill": {
          "email": this.props.auth.user.id
      },
      "notes": {
          "address": "Hello World"
      },
      "theme": {
          "color": "#FF416C"
      }
    };
    var rzp1 = new Razorpay(options);
    rzp1.open();
  }

    previous=()=>{
        this.setState({index:Math.max(0,this.state.index-1)})
    }

    next=()=>{
        this.setState({index:Math.min(this.state.totalImages.length-1,this.state.index+1)})
    }



  render () {
    let {index,totalImages} = this.state;
    let totalAmount = totalImages.length<3?190+(totalImages.length-1)*150:totalImages<=10?440+(totalImages.length-3)*120:totalImages.length>10?1190+(totalImages.length-10)*100:null
    let {photos} = this.props.photo

    let gst = totalAmount-(totalAmount*(100/(100+18)));
          gst = gst.toFixed(2)

    return(<div>
        <ProcessStepper pathname={3}/>
        <div className="row container" id="paymentCategory">
        <h2>Checkout</h2>
        <div className="paymentPhotoWrap">
        <div className="previewImage">
            <div className="previewEditor">
                <div onClick={this.previous} className="previous-gallery" ><img src={Left} alt="left" width="50px"/></div>
                <img id="invitationImage" className="previewImageWrap" src={photos[index].invitationImage} alt=""/>
                <div onClick={this.next} className="next-gallery"><img src={Left} alt="left" width="50px"/></div>
            </div>
          </div>

          <div className="col s12 m6 background">
          <h5 id="title">Order Summary</h5>
          <div className="orderContainer">
            <div className="col s12 m12 " id="orderWrap">
            <div className="col s6 " id="orderText">
              <div className="text_right">Photos - {totalImages.length}</div>
            </div>
            <div className="col s6 " id="orderPrice">
              <div>Rs.{totalAmount}</div>
            </div>
            </div>
            <div className="col s12 m12"  id="line">
            </div>
            <div className="col s12 m12 padding10 margin0">
              <div className="flexSpaceBetween col s12 m12  padding0 margin0">
                <div className="text_right col s6 m6 padding0 margin0">CGST</div>
                <div className="text_right col s6 m6 padding0 margin0">Rs.{(gst/2).toFixed(2)}</div>
              </div>
              <div className="flexRow col s12 m12  padding0 margin0">
                <div className="text_right col s6 m6 padding0 margin0">SGST</div>
                <div className="text_right col s6 m6 padding0 margin0">Rs.{(gst/2).toFixed(2)}</div>
              </div>
              <div className="flexRow col s12 m12  padding0 margin0">
                <div className="h4 text_right col s6 m6 padding0 margin0">Total Amount</div>
                <div className="h4 text_right col s6 m6 padding0 margin0">Rs.{totalAmount}</div>
              </div>
            </div>
            </div>
            <div id="saveAddress" onClick={()=>this.handlePayment({totalAmount,gst,count:totalImages.length,totalImages,_id:this.props.photo._id})}>
                  Payment
            </div>
          </div>

        </div>
    </div>
    </div>)
  }
}

const mapStateToProps = (state) =>{
  return {
    auth:state.auth,
    project:state.project,
    photo:state.photo
  }
}

const mapDispathToProps = (dispatch) => {
  return {
    paymentPhoto: (payment) => {dispatch({type:"PAYMENT_PHOTO",payload:{payment}})},
  }
}


export default connect(mapStateToProps,mapDispathToProps)(Payment)