import React from 'react'
import {connect} from 'react-redux'
import {Input,Row} from 'react-materialize'
import Plus from '../assets/ui/plus.svg'
import Minus from '../assets/ui/minus.svg'
import Info from '../assets/ui/info.svg'
import axios from 'axios';
import { sendPaymentDetails } from '../scripts/uri';
import ProcessStepper from './processStepper';
import Loading from './loading';

const Razorpay = window.Razorpay

class Payment extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading:false,
      videourl:"",
      upload:"",
      otpCheck:"",
      otp:false,
      count:100,
      name:this.props.auth.user.address?this.props.auth.user.address.name:null,
      address:this.props.auth.user.address?this.props.auth.user.address.address:null,
      city:this.props.auth.user.address?this.props.auth.user.address.city:null,
      state:this.props.auth.user.address?this.props.auth.user.address.state:null,
      pincode:this.props.auth.user.address?this.props.auth.user.address.pincode:null,
      phone:this.props.auth.user.address?this.props.auth.user.address.phone:null
    }
  }

  handleInput = (event) =>{
    this.setState({[event.target.id]:event.target.value})
  }



  handlePayment=(address,payment)=>{
    if((address.name&&address.address&&address.pincode&&address.phone)||this.state.count===0)
    {
      var options = {
        "key": "rzp_test_gvCgxv2Z9GOOe0",
        "amount": payment.totalamount*100, // 2000 paise = INR 20
        "name": "Augmo",
        "description": "Purchase Description",
        "image": "../assets/images/augmoLogo.svg",
        "handler": response =>{
          console.log(response)
            if(response.razorpay_payment_id)
            {
              let data = this.props.project
              data.payment = {};
              data.payment = {...data.payment,...payment};
              data.payment.payment=true;
              data.payment.payment_id=response.razorpay_payment_id;
              console.log(data)
              this.setState({loading:true});
              axios.post(sendPaymentDetails,{data,address}).then(res=>{
                if(res.data.status==="success")
                {
                  this.setState({loading:false});
                  this.props.saveAddress(address,payment)
                  this.props.history.push("my_orders")
                }else{
                  this.setState({loading:false});
                  this.props.saveAddress(address,payment)
                  this.props.history.push("my_orders")
                }
              })
            }else{
              alert("Payment failed if money debited from your account it will be refunded in 7 business days")
            }
        },
        "prefill": {
            "name": address.name,
            "email": this.props.auth.user.id
        },
        "notes": {
            "address": "Hello World"
        },
        "theme": {
            "color": "#FF416C"
        }
      };
      var rzp1 = new Razorpay(options);
      rzp1.open();
    }else{
      alert("Please fill the required fields")
    }
  }



  render () {
    let {name,address,city,state,pincode,phone} = this.state;
    let invitationInfoCardPrice = Math.max(0,((this.state.count-100)*2));

    // let invitationInfoCardPrice = this.props.project.invitationPrice.plan==="premium"?Math.max(0,((this.state.count-100)*5)):this.state.count*5;
    let totalamount = 2990+invitationInfoCardPrice;

    let gst = totalamount-(totalamount*(100/(100+18)));
          gst = gst.toFixed(2)

    return(<div>
        {this.state.loading?<Loading/>:null}
        <ProcessStepper pathname={4}/>
        <div className="row container" id="paymentCategory">
        <h2>Checkout</h2>
        <div className="col s12">
        <div className="col s12 m6">
          {this.state.count!==0?
            <div className="col s12 m12 background">
              <h5 id="title">Delivery Address</h5>
              <Row>
                <Input s={12} id="name" labelClassName="" label="Name*" type="text" defaultValue={name} onChange={this.handleInput}/>
                <Input s={12} id="address" labelClassName="" label="Address*" type="text" defaultValue={address} onChange={this.handleInput}/>
                <Input s={6} id="city" labelClassName="" label="City" type="text" defaultValue={city} onChange={this.handleInput}/>
                <Input s={6} id="state" labelClassName="" label="State" type="text" defaultValue={state} onChange={this.handleInput}/>
                <Input s={6} id="pincode" labelClassName="" label="Pin code*" type="text" defaultValue={pincode} onChange={this.handleInput}/>
                <Input s={6} id="phone" labelClassName="" label="Phone*" type="text" defaultValue={phone} onChange={this.handleInput} required/>
            </Row>
            </div>
            :null}
          </div>

          <div className="col s12 m6 background">
          <h5 id="title">Order Summary</h5>
          <div className="orderContainer">
            <div className="col s12 m12 " id="orderWrap">
            {/* <div className="col s2 " id="orderImage">
              <img src={this.props.project.invitationImage} alt="product" width="100%"/>
              <div>edit</div>
            </div> */}
            <div className="col s6 " id="orderText">
              <div className="text_right">Augmo Invitation - Elite</div>
            </div>
            <div className="col s6 " id="orderPrice">
              <div>Rs.{2990}</div>
            </div>
            </div>
            <div className="col s12 m12 " id="orderWrap">
            {/* <div className="col s2 " id="orderImage">
              <img src={this.props.project.invitationImage} alt="product" width="100%"/>
              <div>edit</div>
            </div> */}
            <div className="col s6 " id="orderText">
              <div className="text_right">Invitation Info Card<sup><img src={Info} className="info" alt="help" width="20px"/></sup></div>
            </div>
            <div className="col s6" id="orderPrice">
              <div>Rs.{invitationInfoCardPrice}</div>
            </div>
            <div className="col s6 " id="infoCardCounterWrap">
              <div id="minus" onClick={()=>this.setState({count:Math.max(0,this.state.count-100)})}><img src={Minus} alt="help" width="20px"/></div>
              <div id="count">{this.state.count}</div>
              <div id="add" onClick={()=>this.setState({count:this.state.count+100})}><img src={Plus} alt="help" width="20px"/></div>
            </div>
            </div>

            <div className="col s12 m12"  id="line">
            </div>
            <div className="col s12 m12 padding10 margin0">
              <div className="flexSpaceBetween col s12 m12  padding0 margin0">
                <div className="text_right col s6 m6 padding0 margin0">CGST</div>
                <div className="text_right col s6 m6 padding0 margin0">Rs.{(gst/2).toFixed(2)}</div>
              </div>
              <div className="flexRow col s12 m12  padding0 margin0">
                <div className="text_right col s6 m6 padding0 margin0">SGST</div>
                <div className="text_right col s6 m6 padding0 margin0">Rs.{(gst/2).toFixed(2)}</div>
              </div>
              <div className="flexRow col s12 m12  padding0 margin0">
                <div className="h4 text_right col s6 m6 padding0 margin0">Total Amount</div>
                <div className="h4 text_right col s6 m6 padding0 margin0">Rs.{totalamount}</div>
              </div>
            </div>
            </div>
            <div id="saveAddress" onClick={()=>this.handlePayment({name,address,city,state,pincode,phone},{invitationPrice:2990,invitationInfoCardPrice,totalamount,gst})}>
                  Payment
            </div>
          </div>

        </div>
    </div>
    </div>)
  }
}

const mapStateToProps = (state) =>{
  return {
    auth:state.auth,
    project:state.project
  }
}

const mapDispathToProps = (dispatch) => {
  return {
    saveAddress: (address,payment) => {dispatch({type:"SAVE_ADDRESS",payload:{address,payment}})},
  }
}


export default connect(mapStateToProps,mapDispathToProps)(Payment)