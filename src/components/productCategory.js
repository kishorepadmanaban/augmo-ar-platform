import React from 'react'
import {connect} from 'react-redux'
import Invitation from '../assets/ui/Invitation.svg'
import Photoalbum from '../assets/ui/Camera.svg'
import ProcessStepper from './processStepper';

class productCategory extends React.Component {
  constructor(props) {
    super(props);
    this.state = {

    }
  }

  handleChange = (event) => {
    this.setState({
      [event.target.id]:event.target.value
    });
  }


  render () {
    return(
        <div className="parentContainer">
            <ProcessStepper pathname={0}/>
            <div>
              <h4 className="center ">Select Your Category</h4>
            </div>
            <div className="row container" id="containerCategory">

                <div id="cardWrap">
                    <div onClick={()=>{this.props.addProductCategory("invitation");this.props.history.push('/name');}} id="card" className="col s3 card">
                        <img id="invitation" src={Invitation} alt="invitation"/>
                    </div>
                    <div id="cardText">
                      <h5 className="center ">Invitation</h5>
                    </div>
                  </div>    
                <div id="cardWrap">
                    <div onClick={()=>{this.props.addProductCategoryPhoto("photo");this.props.history.push('/photo_editor');}} id="card"  className="col s3 card">
                        <img id="photoalbum" src={Photoalbum} alt="photoalbum"/>
                    </div>
                    <div id="cardText">
                      <h5 className="center ">Moments</h5>
                    </div>
                </div>
                <div id="cardWrap">
                    <div onClick={()=>{this.props.addProductCategoryPhoto("photo");this.props.history.push('/photo_editor');}} id="card"  className="col s3 card">
                        <img id="photoalbum" src={Photoalbum} alt="event"/>
                    </div>
                    <div id="cardText">
                      <h5 className="center ">Event</h5>
                    </div>
                </div>

            </div>
        </div>

    )
  }
}


const mapStateToProps = (state) => {
  return {project: state.project}
}

const mapDispathToProps = (dispatch) => {
  return {
    addProductCategory: (productCategory) => {dispatch({type:"ADD_PRODUCT_CATEGORY", payload:{productCategory}})},
    addProductCategoryPhoto: (productCategory) => {dispatch({type:"ADD_PRODUCT_CATEGORY_PHOTO", payload:{"productCategory":productCategory,target:[]}})}
  }
}


export default connect(mapStateToProps,mapDispathToProps)(productCategory)
