import React from 'react'
import {connect} from 'react-redux'
import Tick from '../assets/ui/tick.svg'
import ProcessStepper from './processStepperPhoto';
import Left from "../assets/ui/left-arrow.svg"

class Preview extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      upload: "",
      post: "",
      progress: 0,
      imgWidth: 100,
      range:50,
      index:0,
      totalImages:[]
    }
  }

  componentDidMount(){
      let totalImages = this.props.photo.photos.filter(photo=>photo.published===true)
      this.setState({totalImages})
  }

  handleNext = (event) => {
    event.preventDefault();
    // this.props.addInvitation(this.state.invitationImageURL, this.state.invitationVideoURL);
    this.props.history.push('/login');
  }

  previous=()=>{
      this.setState({index:Math.max(0,this.state.index-1)})
  }

  next=()=>{
    this.setState({index:Math.min(this.state.totalImages.length-1,this.state.index+1)})
}

  render() {
    let {photos} = this.props.photo
    let {index} = this.state
    return (<div>
        <ProcessStepper pathname={2}/>
        <div className="previewContainer row container">
        <div className="col s6">
          <div>
            <h4 id="previewHeading">Preview</h4>
          </div>
          <div id="Preview">
            <div id="previewTick">
              <img src={Tick} alt="Next" width="15px"/>
            </div>
            <h5>Download Augmo App</h5>
          </div>
          <div id="Preview">
            <div id="previewTick">
              <img src={Tick} alt="Next" width="15px"/>
            </div>          
            <h5>Aim and Focus the Invitation</h5>
          </div>
          <div id="Preview">
            <div id="previewTick">
                <img src={Tick} alt="Next" width="15px"/>
            </div>          
            <h5>See the Invitation comes to life</h5>
          </div>

          </div>
          <div className="previewImage">
            <div className="previewEditor">
                <div onClick={this.previous} className="previous-gallery" ><img src={Left} alt="left" width="50px"/></div>
                <img id="invitationImage" className="previewImageWrap" src={photos[index].invitationImage} alt=""/>
                <div onClick={this.next} className="next-gallery"><img src={Left} alt="left" width="50px"/></div>
            </div>
          </div>
          {
            this.props.photo.payment.payment?null:
            <div>
              <div onClick={()=>{this.props.history.push("/payment_photo")}} id="publishBtn">
                  Payment
              </div>
              <div onClick={()=>this.props.history.push("/photo_editor")} id="publishBtn">
              Edit
            </div>
          </div>
          }
          
        </div>

    </div>)
  }
}

const mapStateToProps = (state) => {
  return {
    project: state.project,
    photo:state.photo
  }
}


export default connect(mapStateToProps, null)(Preview)
