import React from 'react'
import {connect} from 'react-redux'
import 'react-toastify/dist/ReactToastify.css';
import { sendMetadata } from '../scripts/uri';
import axios from 'axios'
import ProcessStepper from './processStepper';
import Loading from './loading';
import Default from '../assets/images/default.jpg';
import template1 from '../assets/images/template1.jpg';
import template2 from '../assets/images/template2.jpg'
import template3 from '../assets/images/template3.jpg'
import template4 from '../assets/images/template4.jpg'

class Template extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      template:this.props.project.template?this.props.project.template:"",
      loading:false,
      image:this.props.project.template?this.props.project.template[3]:Default
    }
  }

  sendMetadataToVuforia= (event) => {
    event.preventDefault();

    if(this.state.template === "")
    {
      alert("Please Select one template");
    }else{
      this.setState({loading:true})
      const data = {
        metadata:this.props.project,
        target_id:this.props.project.vuforia.target_record.target_id,
        email:this.props.auth.user.id
      }
  
      axios.post(sendMetadata, data).then(response => {
        console.log(response.data);
        if (response.data.status === 'success')
        {
          this.setState({loading:false})
          this.props.published()
          this.props.history.push({
            pathname: '/preview',
            state: { productCategory: "invitation" }
          });
        }
      }).catch(error => {
        console.log(error);
      })
    }
  }

  changeTemplate = (value) =>{
    this.setState({template:value,image:value[3]})
    this.props.addTemplate(value,this.props.project.payment===true?true:false)
  }

  render () {
    let template = [["Augmo Coral","http://augmo.net/src/ios/invitationdefault","http://augmo.net/src/android/invitationdefault",Default], ["Floral Blue","http://augmo.net/src/ios/template1","http://augmo.net/src/android/template1",template1], ["Sandy Biege","http://augmo.net/src/ios/template2","http://augmo.net/src/android/template2",template2], ["Floral Indigo","http://augmo.net/src/ios/template3","http://augmo.net/src/android/template3",template3], ["Royal Ruby","http://augmo.net/src/ios/template4","http://augmo.net/src/android/template4",template4]]
    return(
      <div>
        {this.state.loading?<Loading/>:null}
        <ProcessStepper pathname={2}/>
        <div className="container">

          <h4 className="">Select Your Template</h4>
          <div className="templateContainer row">
          <div className="containerCategory col s12 m6">
            {template.map((value,index)=>
              <div onClick={()=>{this.changeTemplate(value)}} key={index} style={{background:this.state.template[0] === value[0]?"linear-gradient(to right, #FF4B2B, #FF416C)":"white",color:this.state.template[0] === value[0]?"white":"black"}} id="templateCard" className="col s3">
                  <div>{value[0]}</div>
              </div>
            )}
          </div>
          <div className="col s12 m6 templatePreview">
              <img src={this.state.image} alt="template" className="template"/>
          </div>
          </div>
            <div onClick={(event)=>this.sendMetadataToVuforia(event)} id="publishBtn">
                Publish & Preview
            </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    project: state.project,
    auth:state.auth
  }
}

const mapDispathToProps = (dispatch) => {
  return {
    addTemplate: (template,payment) => {dispatch({type:"ADD_TEMPLATE", payload:{template,payment}})},
    published: () => {dispatch({type:"INVITATION_PUBLISHED"})}
  }
}


export default connect(mapStateToProps,mapDispathToProps)(Template)
