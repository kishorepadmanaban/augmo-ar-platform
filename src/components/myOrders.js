import React from 'react'
import {connect} from 'react-redux'
import {NavLink} from 'react-router-dom'
import PersonalInfo from './personalInfo';
import ManageAddress from './manageAddress';
import OrderHistory from './orderHistory';
import BuyInfoCard from './buyInfoCard';
import { deleteAllToCreateNewProject, getData } from '../scripts/uri';
const axios = require('axios');


class MyOrders extends React.Component {
  constructor(props) {
    super(props);
    this.state = {

    }
  }

  componentDidMount(){
    axios.get(getData+this.props.auth.user.id).then(res=>{
      console.log(res.data)
      this.setState({data:res.data})
  })
  }

  createNewProject=(type)=>{
    let body = {
      invitation:this.props.project,
      photo:this.props.photo,
      type:type
    }
    axios.post(deleteAllToCreateNewProject,body).then(res=>{
      if(type==="invitation"){
        this.props.clearInvitation();
        this.props.history.push("/product")
      }else if(type==="photo")
      {
        this.props.clearPhoto();
        this.props.history.push("/product")
      }
    })
  }

  createNewProjectConfirm=(type)=>{
    if(window.confirm("Creating new project will delete all unpublished data")===true)
    {
      if(type==="invitation")
      {
        let invitation = this.state.data.filter(invitation=>invitation.metadata.productCategory==="invitation")
        if(this.props.project.vuforia)
        {
          let checkInvitation = invitation.filter(invitation=>invitation.metadata.vuforia.target_record.target_id===this.props.project.vuforia.target_record.target_id)
          if(checkInvitation.length>0)
          {
            this.props.clearInvitation();
            this.props.history.push("/product")
          }else{
            this.createNewProject("invitation");
          }
        }else{
          this.props.clearInvitation();
          this.props.history.push("/product")
        }
      }else if(type==="photo"){
        let photo = this.state.data.filter(photo=>photo.metadata.productCategory==="photo")
        if(this.props.photo.photos)
        {
            if(this.props.photo.photos[0].vuforia)
            {
              let checkPhoto = photo.filter(photo=>photo.metadata.photos[0].vuforia.target_record.target_id===this.props.photo.photos[0].vuforia.target_record.target_id)
              if(checkPhoto.length>0)
              {
                this.props.clearPhoto();
                this.props.history.push("/product")
              }else{
                this.createNewProject("photo");
              }
            }else{
              this.props.clearPhoto();
              this.props.history.push("/product")
            }
          }else{
            this.props.clearPhoto();
            this.props.history.push("/product")
          }
      }
    }
  }


  render () {

    return(<div className="row container padding30">
        <div className="col s12 m4 height">
          <div className="col s12 flexStartColumn myorderUsername"> 
              <div className="h5 text_left">Welcome,</div>
              <div className="h5 bold text_left">{this.props.auth.user?this.props.auth.user.id:null}</div>
          </div>
          <div className="col s12 createNewProject line dropdown">
              <div className="h5 text_left">Create</div>
              <div className="h4 text_left pink-text left">New Project</div>
              <div className="dropdown-contentt">
                <div className="dropdown-data" onClick={()=>this.createNewProjectConfirm("invitation")}>
                  Invitation
                </div>
                <div className="dropdown-data" onClick={()=>this.createNewProjectConfirm("photo")}>
                  Photo
                </div>
              </div>
          </div>
          <div className="col s12 text_left padding20 line">
              <NavLink exact className="h5 " to="/buy_info_card" style={{color:"black"}} activeStyle={{color:"#e91e63",fontWeight:"bold"}}>Buy Info Card</NavLink>
          </div>
          <div className="col s12 text_left padding20 line">
              <NavLink exact className="h5" to="/personal_info" style={{color:"black"}} activeStyle={{color:"#e91e63",fontWeight:"bold"}}>Personal Information</NavLink>
          </div>
          <div className="col s12 text_left padding20 line">
              <NavLink exact className="h5" to="/manage_address" style={{color:"black"}} activeStyle={{color:"#e91e63",fontWeight:"bold"}}>Manage Address</NavLink>
          </div>
          <div className="col s12 text_left padding20 line">
              <NavLink exact className="h5" to="/my_orders"  style={{color:"black"}} activeStyle={{color:"#e91e63",fontWeight:"bold"}}>Order History</NavLink>
          </div>
        </div>
        <div className="col s12 m8 myContent">
        {this.props.location.pathname === "/personal_info"?
        <PersonalInfo/>:null
        }
        {this.props.location.pathname === "/manage_address"?
        <ManageAddress/>:null
        }
        {this.props.location.pathname === "/my_orders"?
        <OrderHistory invitationRoute={()=>this.props.history.push("/preview")} photoRoute={()=>this.props.history.push("/preview_photo")}/>:null
        }
        {this.props.location.pathname === "/buy_info_card"?
        <BuyInfoCard/>:null
        }
        </div>
    </div>)
  }
}

const mapStateToProps = (state) =>{
  return {
    auth:state.auth,
    project:state.project,
    photo:state.photo
  }
}

const mapDispathToProps = (dispatch) => {
  return {
    createNewProject: () => {dispatch({type:"CREATE_NEW_PROJECT"})},
    clearInvitation: () => {dispatch({type:"CLEAR_INVITATION"})},
    clearPhoto: () => {dispatch({type:"CLEAR_PHOTO"})},
  }
}


export default connect(mapStateToProps,mapDispathToProps)(MyOrders)