import React from 'react'
import {connect} from 'react-redux'
import { galleryImages, removeGalleryImage } from '../scripts/uri'
import axios from 'axios'
import ProcessStepper from './processStepper';
import Loading from './loading';
import Compressor from 'compressorjs';


class GalleryImageUpload extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      imageurl:this.props.project.gallery_images?this.props.project.gallery_images:[],
      upload:"",
      loading:false
    }
    this.handleUpload = this.handleUpload.bind(this)
  }

  handleNext = () => {
    this.props.history.push('/template');
  }

  handleUpload (event) {
    event.preventDefault();
    console.log(this.props.project.gallery_images?this.props.project.gallery_images.length:null+event.target.files.length)
    if((this.props.project.gallery_images?this.props.project.gallery_images.length:null+event.target.files.length)>20)
    {
      alert("File exceeded - You can only upload 20 images maximum")
    }else{
      this.setState({loading:true})
      const data = new FormData();

        var promisesToRun = [];

        var compressorAsPromise = function(file, count) {
          return new Promise(function(resolve, reject) {
            new Compressor(file, {
                quality: 0.2,
                maxWidth: 1000,
                success(result) {
                  data.append('galleryImages'+count, result, result.name);
                  // console.log(result)
                  resolve(result);
                }
          })
        })
      }

        for(let i=0;i<event.target.files.length;i++)
        {
          promisesToRun.push(compressorAsPromise(event.target.files[i], i))
        }

        Promise.all(promisesToRun)
        .then(values => {
          // console.log(values)
          const config = {
            onUploadProgress: progressEvent => {
              let progress = (progressEvent.loaded * 80) / progressEvent.total
              this.setState({progress: progress});
            }
          }
          axios.post(galleryImages, data, config).then(response => {
            if (response.data.status === 'success')
            {
      
              this.props.addGalleryImages({gallery_images:response.data.url});
              this.setState({loading:false})
              this.setState({progress: 100});
            }
          }).catch(error => {
            console.log(error);
          })
        })
        .catch(err => {

        })

        // await new Promise(resolve => setTimeout(resolve, event.target.files.length * 2000))


        // let photos = await Promise.all(
        // (event.target.files).map((photo,index)=>
        
        //   new Compressor(photo, {
        //     quality: 0.1,
        //     maxWidth: 1000,
        //     success(result) {
        //       data.append('galleryImages'+[index], result, result.name);
        //       console.log(result)
        //     }
        //   })
        
        // )
        // )
      // const config = {
      //   onUploadProgress: progressEvent => {
      //     let progress = (progressEvent.loaded * 80) / progressEvent.total
      //     this.setState({progress: progress});
      //   }
      // }
      // axios.post(galleryImages, data, config).then(response => {
      //   if (response.data.status === 'success')
      //   {
  
      //     this.props.addGalleryImages({gallery_images:response.data.url});
      //     this.setState({loading:false})
      //     this.setState({progress: 100});
      //   }
      // }).catch(error => {
      //   console.log(error);
      // })
    }

  }

  removeGalleryImage = (event,image) =>{
    event.preventDefault();
    this.setState({loading:true})
    console.log(image)
    let imagePath = image.replace("http://159.65.146.12/invitation/", '')

    console.log(imagePath)

    const data = {
      image:imagePath
    }

    axios.delete(removeGalleryImage, {data}).then(response => {
      if (response.data.status === 'success')
      {
        this.props.removeGalleryImage(image);
        this.setState({progress: 100});
        this.setState({loading:false})

      }
    }).catch(error => {
      console.log(error);
    })
  }


  render () {
    if(this.state.loading){
      document.getElementsByTagName("BODY")[0].style.overflow = "hidden";
    }else{
      document.getElementsByTagName("BODY")[0].style.overflow = "auto";
    }

    return(
      <div>
        {this.state.loading?<Loading/>:null}
        <ProcessStepper pathname={2}/>
        <form className="row container">
          <div className="row">
            <div className="col s12">
              <h4 className="">Upload Images for Gallery</h4>
              {
                this.props.project.gallery_images?this.props.project.gallery_images.map((value,index)=>
                <div key={index} className="col s8 m4 l2" id="galleryContainer">
                  <img id="galleryImages" src={value} alt="" />
                  <button onClick={(event)=>this.removeGalleryImage(event,value)} id="btn">X</button>
                </div>
              ):null
            }
            <div className="fileContainer">
              <div id="plus">+</div>
              <h6>Add images</h6>
              <input id="invitation" type="file" accept="image/*" className="invitation" ref='invitation' multiple onChange={this.handleUpload}/>
            </div>
            </div>
          </div>
          {
          this.props.project.invitationImage !== undefined?
          // <div onClick={this.handleNext} id="nextBtnInvitation">
          //     <img src={NextBtn} alt="Next" width="60px"/>
          //   </div>
          <div onClick={this.handleNext} id="nextBtnNew">
          <b>Next :</b> Choose Template
          </div>
            :null
          }
        </form>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {project: state.project}
}

const mapDispathToProps = (dispatch) => {
  return {
    addGalleryImages: (galleryImages) => {dispatch({type:"ADD_GALLERY_IMAGES", payload:galleryImages})},
    removeGalleryImage: (image) =>{dispatch({type:"REMOVE_GALLERY_IMAGE", payload:image})}
  }
}


export default connect(mapStateToProps,mapDispathToProps)(GalleryImageUpload)
