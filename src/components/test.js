// // Import FirebaseAuth and firebase.
// import React from 'react';
// import StyledFirebaseAuth from 'react-firebaseui/StyledFirebaseAuth';
// import firebase from 'firebase';
 
// // Configure Firebase.
// const config = {
//   apiKey:"AIzaSyBQSbXn-ck46DMZLxF6Ai38cKF7qdO4I_Q",
//   authDomain:"augmo-platform.firebaseapp.com"
//   // ...
// };
// firebase.initializeApp(config);
 
// class SignInScreen extends React.Component {
 
//   // The component's Local state.
//   state = {
//     isSignedIn: false // Local signed-in state.
//   };
 
//   // Configure FirebaseUI.
//   uiConfig = {
//     // Popup signin flow rather than redirect flow.
//     signInFlow: 'popup',
//     // We will display Google and Facebook as auth providers.
//     signInOptions: [
//       firebase.auth.GoogleAuthProvider.PROVIDER_ID,
//       firebase.auth.FacebookAuthProvider.PROVIDER_ID
//     ],
//     callbacks: {
//       // Avoid redirects after sign-in.
//       signInSuccessWithAuthResult: () => false
//     }
//   };
 
//   // Listen to the Firebase Auth state and set the local state.
//   componentDidMount() {
//     this.unregisterAuthObserver = firebase.auth().onAuthStateChanged(
//         (user) => this.setState({isSignedIn: !!user,user:user}),
        
//     );
//   }
  
//   // Make sure we un-register Firebase observers when the component unmounts.
//   componentWillUnmount() {
//     this.unregisterAuthObserver();
//   }
 
//   render() {
//     console.log(this.state.user)
//     if (!this.state.isSignedIn) {
//       return (
//         <div>
//           <h1>My App</h1>
//           <p>Please sign-in:</p>
//           <StyledFirebaseAuth uiConfig={this.uiConfig} firebaseAuth={firebase.auth()}/>
//         </div>
//       );
//     }
//     return (
//       <div>
//         <h1>My App</h1>
//         <p>Welcome {firebase.auth().currentUser.displayName}! You are now signed-in!</p>
//         <div onClick={() => firebase.auth().signOut()}>Sign-out</div>
//       </div>
//     );
//   }
// }

// export default SignInScreen;


// 'use strict';

// import React, { Component } from 'react';
// import Stepper from 'react-stepper-horizontal'

// export default class Step extends Component {
//   constructor() {
//     super();
//     this.state = {
//       steps: [{
//         title: 'Step One',
//         href: 'http://example1.com',
//         onClick: (e) => {
//           e.preventDefault()
//           console.log('onClick', 1)
//         }
//       }, {
//         title: 'Step Two',
//         href: 'http://example2.com',
//         onClick: (e) => {
//           e.preventDefault()
//           console.log('onClick', 2)
//         }
//       }, {
//         title: 'Step Three (Disabled)',
//         href: 'http://example3.com',
//         onClick: (e) => {
//           e.preventDefault()
//           console.log('onClick', 3)
//         }
//       }, {
//         title: 'Step Four',
//         href: 'http://example4.com',
//         onClick: (e) => {
//           e.preventDefault()
//           console.log('onClick', 4)
//         }
//       }],
//       currentStep: 0,
//     };
//     this.onClickNext = this.onClickNext.bind(this);
//   }

//   onClickNext() {
//     const { steps, currentStep } = this.state;
//     this.setState({
//       currentStep: currentStep + 1,
//     });
//   }

//   render() {
//     const { steps, currentStep } = this.state;
//     const buttonStyle = { background: '#E0E0E0', width: 200, padding: 16, textAlign: 'center', margin: '0 auto', marginTop: 32 };

//     return (
//       <div>
//         <Stepper steps={ steps } activeStep={ currentStep } />
//         <div style={ buttonStyle } onClick={ this.onClickNext }>Next</div>
//       </div>
//     );
//   }
// };

// import React from 'react'
// import {connect} from 'react-redux'
// import {addPhotoImage, addPhotoVideo, removePhotoImage, removePhotoVideo} from '../scripts/uri'
// import axios from 'axios'
// import StarRatings from 'react-star-ratings';
// import NextBtn from '../assets/ui/Arrow with button.svg'
// import Tick from '../assets/ui/tick.svg'
// import Cloud from '../assets/ui/Cloud.svg'
// import Loading from './loading';

// class PhotoComponent extends React.Component {
//   constructor(props) {
//     super(props);
//     this.state = {
//       upload: "",
//       progress: 0,
//       targetWidth: this.props.photo.targetWidth?this.props.photo.targetWidth:null,
//       range:this.props.photo.scale?this.props.photo.scale:100,
//       value:5,
//       position:this.props.photo.position?this.props.photo.position:[0,0],
//       photoWidth:null,
//       photoHeight:null
//     }
//   }

//   componentDidMount(){
//     if(this.props.project.invitationVideo && this.props.project.position){
//       var dragItem = document.querySelector("#item");
//       dragItem.style.transform = "translate3d(" + this.props.project.position[0] + "px, " + (-this.props.project.position[1]) + "px, 0)";
//     }
//     this.dragElement()
//   }


//   slider = () => {
//     var slider = document.getElementById("test5");
//     this.setState({range:slider.value})
//   }



//   dragElement = () => {
//     var img = document.getElementById("photoImage");
//     this.setState({imgWidth: img.clientWidth});

//     var dragItem = document.querySelector("#item");
//     var container = document.querySelector("#container");
//     var position = document.querySelector("#position");
//     var photoImage = document.querySelector("#photoImage");

//     if(container)
//     {

//     var active = false;
//     var currentX;
//     var currentY;
//     var initialX;
//     var initialY;
//     var xOffset = this.props.photo.position?this.props.photo.position[0]:0;
//     var yOffset = this.props.photo.position?-this.props.photo.position[1]:0;

  
//     container.addEventListener("touchstart", dragStart, false);
//     container.addEventListener("touchend", dragEnd, false);
//     container.addEventListener("touchmove", drag, false);
  
//     container.addEventListener("mousedown", dragStart, false);
//     container.addEventListener("mouseup", dragEnd, false);
//     container.addEventListener("mousemove", drag, false);
//     container.addEventListener("mouseout", dragEnd, false);

//     photoImage.addEventListener("touchstart", dragStart, false);
//     photoImage.addEventListener("touchend", dragEnd, false);
//     photoImage.addEventListener("touchmove", drag, false);
  
//     photoImage.addEventListener("mousedown", dragStart, false);
//     photoImage.addEventListener("mouseup", dragEnd, false);
//     photoImage.addEventListener("mousemove", drag, false);
  
//     function dragStart(e) {
//       if (e.type === "touchstart") {
//         initialX = e.touches[0].clientX - xOffset;
//         initialY = e.touches[0].clientY - yOffset;
//       } else {
//         initialX = e.clientX - xOffset;
//         initialY = e.clientY - yOffset;
//       }
  
//       if (e.target === dragItem) {
//         active = true;
//       }
//     }
  
//     function dragEnd(e) {
//       initialX = currentX;
//       initialY = currentY;
//       active = false;
//     }
  
//     function drag(e) {
//       if (active) {
      
//         e.preventDefault();
      
//         if (e.type === "touchmove") {
//           currentX = e.touches[0].clientX - initialX;
//           currentY = e.touches[0].clientY - initialY;
//         } else {
//           currentX = e.clientX - initialX;
//           currentY = e.clientY - initialY;
//         }
  
//         xOffset = currentX;
//         yOffset = currentY;
  
//         setTranslate(currentX, currentY, dragItem);
//       }
//     }

  
//     var setTranslate=(xPos, yPos, el)=> {
//       el.style.transform = "translate3d(" + xPos + "px, " + yPos + "px, 0)";
//       position.innerHTML = xPos+","+yPos;
//       this.setState({position:[xPos,-yPos]})
//     }
//   }
//   }



//   handleNext = (event) => {
//     event.preventDefault();
//     event.preventDefault();
//     var img = document.getElementById("photoImage");

//     this.props.scaleAndPosition(parseInt(this.state.range,10),this.state.position,img.naturalWidth,img.naturalHeight);
//     this.props.history.push('/gallery');
//   }

//   handleImageUpload = (event) => {
//     event.preventDefault();
//     const data = new FormData();
//     data.append('invitationImage', event.target.files[0]);

//     const config = {
//       onUploadProgress: progressEvent => {
//         let progress = (progressEvent.loaded * 66) / progressEvent.total
//         this.setState({progress: progress});
//       }
//     }
//     this.setState({loading:true})

//     axios.post(addPhotoImage, data, config).then(response => {
//       this.props.addPhotoImage(response.data.image,response.data.response,this.props.count);
//       this.setState({progress: 100,loading:false});

//     }).catch(error => {
//       console.log(error);
//     })
//   }

//   handleVideoUpload = (event) => {
//     event.preventDefault();

//     const data = new FormData();
//     data.append('invitationVideo', event.target.files[0]);

//     const config = {
//       onUploadProgress: progressEvent => {
//         let progress = (progressEvent.loaded * 83) / progressEvent.total
//         this.setState({
//           progress: progress
//         });
//       }
//     }
//     this.setState({loading:true})

//     axios.post(addPhotoVideo, data, config).then(response => {
//       console.log(response.data);
//       var img = document.getElementById("photoImage");

//       this.setState({
//         progress: 100,
//         loading:false,
//         status: response.data.status,
//         imgWidth: img.clientWidth});
//       this.props.addPhotoVideo(response.data.url,this.props.count);
//       this.props.addTargetWidth(img.clientWidth);

//       this.dragElement();

//     }).catch(error => {
//       console.log(error);
//     })
//   }

//   removePhotoVideo = (event,video) =>{
//     event.preventDefault();
//     let videoPath = video.replace("http://159.65.146.12/photo/", '')

//     const data = {
//       image:videoPath
//     }

//     axios.delete(removePhotoVideo, {data}).then(response => {
//       if (response.data.status === 'success')
//       {

//         this.props.removePhotoVideo(this.props.count);
//         this.setState({loading: false});
//         this.setState({progress: 100,position:[0,0],scale:100});

//       }
//     }).catch(error => {
//       console.log(error);
//     })
//   }


//   removePhotoImage = (event) =>{
//     this.setState({loading:true})

//     axios.delete(removePhotoImage+this.props.photo.target[this.props.count].vuforia.target_record.target_id).then(response => {
//       this.setState({loading:false})
//       this.props.removePhotoImage(this.props.count);
//     }).catch(error => {
//       console.log(error);
//     })
//   }



//   render() {
//     return (<div className="row  container">
//         <div className="row">
//           <div className="col s7 Editor">
//           <p className=" videoInfo">
//             Hold and drag the video to position on the photo
//           </p>
//             <div className="white imageEditor">
//               <img id="photoImage" className="photoImage" src={this.props.photo.target[this.props.count]!==undefined?this.props.photo.target[this.props.count].photoImage:null} alt=""/>
//               {
//                 this.props.photo.target[this.props.count] === undefined
//                   ? null
//                   : <div id="container"><video id="item" width={this.state.imgWidth*(this.state.range/100)} controls="controls">
//                       <source src={this.props.photo.target[this.props.count].photoVideo} type="video/mp4"/>
//                       Your browser does not support the video tag.
//                     </video></div>
//               }
//             </div>
//             <div className="col s11 sliderContainer">
//             <p className="col s3  resizeVideo">Resize Video:</p>
//               <p className="range-field col s7">
//                 <input className="" type="range" id="test5" min="1" max="100" onInput={this.slider}/>
//               </p>
//             <p id="position"></p>
//             </div>
//           </div>
//           <div className="input-field col s5">
//             <h4>Upload Details</h4>
//             <div>
//             <div id="invitationWrap">
//               <div>
//                 <h5>Upload Photo</h5>
//               </div>
//               <div>
//                 {
//                   this.props.photo.target[this.props.count] === undefined
//                   ? <div id="invitationDiv"><input id="invitation" type="file" accept="image/*" className="invitation" ref='photo' onChange={this.handleImageUpload}/>
//                   {
//                     !this.state.loadingPhoto?<img id="upload" src={Cloud} alt="upload"/>:<p  id="progress">{Math.round(this.state.progress)}%</p>
//                   }
//                   </div>:
//                   <div id="tick">
//                   <img src={Tick} alt="Next" width="25px"/>
//                   </div>
//                 }
//               </div>
//             </div>
//               <div>
//                 {
//                 this.props.photo.target[this.props.count] !== undefined
//                 ?
//                  <div id="close"><p className="">Photo</p>
//               <button onClick={(event)=>this.removePhotoImage(event)} id="closebtn">X</button></div>:null
//                 }
//                 {
//                   this.props.photo.target[this.props.count] === undefined
//                     ? null
//                     : <div className="rating">
//                         <StarRatings starEmptyColor="rgba(203, 211, 227, 0.3)" rating={this.props.photo.target[this.props.count].vuforia.target_record.tracking_rating} numberOfStars={5} starDimension="30px" starRatedColor="#FF416C" starSpacing="3px"/></div>
//                 }
//               </div>
//             </div>

//             {
//               this.props.photo.target[this.props.count] === undefined?null:
//               <div>
//                 <div id="invitationVideoWrap">
//                   <div>
//                   <h5>Upload Photo video</h5>
//                   </div>
//                   <div>
//                   {
//                     this.props.photo.target[this.props.count] === undefined
//                     ? <div id="invitationDiv"><input id="invitation" type="file" accept="video/*" className="invitation"  onChange={this.handleVideoUpload}/>
//                     {
//                       !this.state.loadingPhotoVideo?<img id="upload" src={Cloud} alt="upload"/>:<p id="progress">{Math.round(this.state.progress)}%</p>
//                     }
                    
                    
//                     </div>
//                   : <div id="tick">
//                   <img src={this.state.loadingPhotoVideo?Cloud:Tick} alt="Next" width="25px"/>
//                   </div>
//                   }
//                 </div>
//               </div>
//                 <div>
//                 {
//                   this.props.photo.target[this.props.count].photoVideo !== undefined ?
//                   <div id="close"><p className="">Photo video</p>
//                     <button onClick={(event)=>this.removePhotoVideo(event,this.props.photo.target[this.props.count].photoVideo)} id="closebtn">X</button></div>:null
//                 }
//               </div>
//               </div>
//             }
//           </div>
//         </div>
//     </div>)
//   }
// }

// const mapStateToProps = (state) => {
//   return {photo: state.photo}
// }

// const mapDispathToProps = (dispatch) => {
//   return {
//     addPhotoImage: (photoImage,vuforia,count) => {
//       dispatch({
//         type: "ADD_PHOTO_IMAGE",
//         payload: {data:[{
//           photoImage,
//           vuforia
//         }],count:count}
//       })
//     },
//     addPhotoVideo: (photoVideo,count) => {
//       dispatch({
//         type: "ADD_PHOTO_VIDEO",
//         payload: {
//           photoVideo:photoVideo,
//           count:count
//         }
//       })
//     },
//     removePhotoVideo: (count) => {
//       dispatch({
//         type: "REMOVE_PHOTO_VIDEO",
//         payload:{count:count}
//       })
//     },
//     removePhotoImage: (count) => {
//       dispatch({
//         type: "REMOVE_PHOTO_IMAGE",
//         payload:{count:count}
//       })
//     },
//     scaleImage: (scale,count) => {
//       dispatch({
//         type: "SCALE_AND_POSITION",
//         payload:{
//           scale:scale,
//           count:count
//         }
//       })
//     },
//     positionImage: (position,count) => {
//         dispatch({
//           type: "SCALE_AND_POSITION",
//           payload:{
//             position:position,
//             count:count
//           }
//         })
//       }
//   }
// }

// export default connect(mapStateToProps, mapDispathToProps)(PhotoComponent)



import React from 'react'
import {connect} from 'react-redux'
import {addInvitationImage, addInvitationVideo, removeInvitationImage, removeInvitationVideo} from '../scripts/uri'
import axios from 'axios'
import StarRatings from 'react-star-ratings';
import { sendMetadata } from '../scripts/uri';
import Tick from '../assets/ui/tick.svg'
import Cloud from '../assets/ui/Cloud.svg'
import ProcessStepper from './processStepper';
import Loading from './loading';

class PhotoUpload extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      upload: "",
      progress: 0,
      targetWidth: this.props.photo.targetWidth?this.props.photo.targetWidth:null,
      range:this.props.photo.scale?this.props.photo.scale:100,
      value:5,
      position:this.props.photo.position?this.props.photo.position:[0,0],
      invitationWidth:null,
      invitationHeight:null
    }
  }

  componentDidMount(){

    if(this.props.photo.invitationVideo && this.props.photo.position){
      var dragItem = document.querySelector("#item");
      dragItem.style.transform = "translate3d(" + this.props.photo.position[0] + "px, " + (-this.props.photo.position[1]) + "px, 0)";
    }
      this.dragElement();
  }


  slider = () => {
    var slider = document.getElementById("test5");
    this.setState({range:slider.value})
  }



  dragElement = () => {
    var dragItem = document.querySelector("#item");
    var container = document.querySelector("#container");
    var position = document.querySelector("#position");
    var invitationImage = document.querySelector("#invitationImage");
    if(container)
    {

    var active = false;
    var currentX;
    var currentY;
    var initialX;
    var initialY;
    var xOffset = this.props.photo.position?this.props.photo.position[0]:0;
    var yOffset = this.props.photo.position?-this.props.photo.position[1]:0;

    container.addEventListener("touchstart", dragStart, false);
    container.addEventListener("touchend", dragEnd, false);
    container.addEventListener("touchmove", drag, false);
  
    container.addEventListener("mousedown", dragStart, false);
    container.addEventListener("mouseup", dragEnd, false);
    container.addEventListener("mousemove", drag, false);
    container.addEventListener("mouseout", dragEnd, false);

    invitationImage.addEventListener("touchstart", dragStart, false);
    invitationImage.addEventListener("touchend", dragEnd, false);
    invitationImage.addEventListener("touchmove", drag, false);
  
    invitationImage.addEventListener("mousedown", dragStart, false);
    invitationImage.addEventListener("mouseup", dragEnd, false);
    invitationImage.addEventListener("mousemove", drag, false);
  
    function dragStart(e){
      if (e.type === "touchstart") {
        initialX = e.touches[0].clientX - xOffset;
        initialY = e.touches[0].clientY - yOffset;
      } else {
        initialX = e.clientX - xOffset;
        initialY = e.clientY - yOffset;
      }
  
      if (e.target === dragItem) {
        active = true;
      }
    }
  
    function dragEnd(e) {
      initialX = currentX;
      initialY = currentY;
      active = false;
    }
  
    function drag(e) {
      if (active) {
      
        e.preventDefault();
      
        if (e.type === "touchmove") {
          currentX = e.touches[0].clientX - initialX;
          currentY = e.touches[0].clientY - initialY;
        } else {
          currentX = e.clientX - initialX;
          currentY = e.clientY - initialY;
        }
  
        xOffset = currentX;
        yOffset = currentY;
  
        setTranslate(currentX, currentY, dragItem);
      }
    }
  
    var setTranslate=(xPos, yPos, el)=> {
      el.style.transform = "translate3d(" + xPos + "px, " + yPos + "px, 0)";
      position.innerHTML = xPos+","+yPos;
      this.setState({position:[xPos,-yPos]})
    }
  }
  }



  handleNext = (event) => {
    event.preventDefault();
    const data = {
      metadata:this.props.photo,
      target_id:this.props.photo.vuforia.target_record.target_id
    }
    this.setState({loading:true})
    axios.post(sendMetadata, data).then(response => {
      console.log(response.data);
      if (response.data.status === 'success')
      {
        this.setState({loading:false})
        this.props.history.push({
          pathname: '/preview',
          state: { productCategory: "photo" }
        });
      }
    }).catch(error => {
      console.log(error);
    })  
  }

  checkImage=(event)=>{
    event.persist();
    var reader = new FileReader();
    //Read the contents of Image File.
    reader.readAsDataURL(event.target.files[0]);
    reader.onloadend = (e) =>{
    var image = new Image();
    image.src = e.currentTarget.result;
    
    image.onload = e =>{
      //Determine the Height and Width.
      var height = e.currentTarget.width;
      var width = e.currentTarget.width;
      console.log(height,width)
      if (height < 400 && width < 400) {
          alert("Height or Width should be greater than 400px.");
          }else{
            this.handleImageUpload(event);
          }
        }
      };
  }

   handleImageUpload = (event)=> {
    if(event.target.files[0].size>2097152)
    {
      alert("File size exceeded - Please upload image less than 2MB in size.")
    } else{

    this.setState({loadingInvitation:true})
    const data = new FormData();
    data.append('invitationImage', event.target.files[0]);
    console.log(event.target.files[0])
    const config = {
      onUploadProgress: progressEvent => {
        let progress = (progressEvent.loaded * 66) / progressEvent.total
        this.setState({progress: progress});
      }
    }
    this.setState({loading:true})
    axios.post(addInvitationImage, data, config).then(response => {
      console.log(response.data);
      if(response.data.status==="success")
      {
        this.setState({loading:false})
        this.setState({progress: 100,loadingInvitation:false});
        let extendedTracking = true;
        let template = ["default","http://augmo.net/src/invitationdefault","http://augmo.net/src/invitationdefault"];

        this.props.addInvitationImage(response.data.image,response.data.response,extendedTracking,template);

      }else if(response.data.error==="Bad Image"){
        this.setState({loading:false})

        alert("Bad Image - Please upload some other image since this image is not suitable for augmentation")

      }else if(response.data.error==="Duplicate target exist"){
        this.setState({loading:false})

        alert("Duplicate image exist - Please upload any other image since this image is already exists in our database")

      }
    }).catch(error => {
      console.log(error);
    })
  }
}

  handleVideoUpload = (event) => {
    this.setState({loadingInvitationVideo:true})

    const data = new FormData();
    data.append('invitationVideo', event.target.files[0]);

    const config = {
      onUploadProgress: progressEvent => {
        let progress = (progressEvent.loaded * 83) / progressEvent.total
        this.setState({
          progress: progress
        });
      }
    }
    this.setState({loading:true})

    axios.post(addInvitationVideo, data, config).then(response => {
      this.setState({loading:false})
      var img = document.getElementById("invitationImage");
      this.setState({
        progress: 100,
        loadingInvitationVideo:false,
        status: response.data.status,
        targetWidth: img.clientWidth,
      });
      this.props.scaleAndPosition(parseInt(this.state.range,10),this.state.position,img.naturalWidth,img.naturalHeight);
      this.props.addInvitationVideo(response.data.url);
      this.props.addTargetWidth(img.clientWidth);
      this.dragElement();

    }).catch(error => {
      console.log(error);
    })
  }

  removeInvitationVideo = (event,video) =>{
    let videoPath = video.replace("http://159.65.146.12/invitation/", '')
    const data = {
      image:videoPath
    }

    this.setState({loading:true})
    axios.delete(removeInvitationVideo, {data}).then(response => {
      if (response.data.status === 'success')
      {
        this.setState({loading:false})
        this.setState({progress: 100,position:[0,0],scale:100});
        this.props.removeInvitationVideo();
        this.props.resetPositionAndScale();
      }
    }).catch(error => {
      console.log(error);
    })
  }


  removeInvitationImage = (event) =>{
    this.setState({loading:true})
    axios.delete(removeInvitationImage+this.props.photo.vuforia.target_record.target_id).then(response => {
      this.setState({loading:false})
      if(response.data.result_code==="Success")
      {
        this.props.removeInvitationImage();
      }
    }).catch(error => {
      console.log(error);
    })
  }



  render() {

    if(this.state.loading){
      document.getElementsByTagName("BODY")[0].style.overflow = "hidden";
    }else{
      document.getElementsByTagName("BODY")[0].style.overflow = "auto";
    }


    let videoWidth= (this.state.targetWidth*(this.state.range/100)).toString()+"px"
    return (<div>
      {this.state.loading?<Loading/>:null}
        <ProcessStepper pathname={2}/>
        <div className="row container">
          <div className="input-field col s5">
            <h4>Upload Details</h4>
            <div>
            <div id="invitationWrap">
              <div>
                <h5>Upload Photo</h5>
              </div>
              <div>
                {
                  this.props.photo.invitationImage === undefined
                  ? <div id="invitationDiv"><input id="invitation" type="file" accept="image/*" className="invitation" ref='invitation' onChange={this.checkImage}/>
                  <img id="upload" src={Cloud} alt="upload"/>
                  </div>:
                  <div id="tick">
                  <img src={Tick} alt="Next" width="25px"/>
                  </div>
                }
              </div>
            </div>
              <div>
                {
                this.props.photo.invitationImage !== undefined
                ?
                 <div id="close"><p className="">Invitation</p>
              <button onClick={(event)=>this.removeInvitationImage(event)} id="closebtn">X</button></div>:null
                }
                {
                  this.props.photo.vuforia === undefined
                    ? null
                    : <div className="rating">
                        <StarRatings starEmptyColor="rgba(203, 211, 227, 0.3)" rating={this.props.photo.vuforia.target_record.tracking_rating} numberOfStars={5} starDimension="20px" starRatedColor="#FF416C" starSpacing="3px"/></div>
                }
              </div>
            </div>

            {
              this.props.photo.invitationImage === undefined?null:
              <div>
                <div id="invitationVideoWrap">
                  <div>
                  <h5>Upload Video</h5>
                  </div>
                  <div>
                  {
                    this.props.photo.invitationVideo === undefined
                    ? <div id="invitationDiv"><input id="invitation" type="file" accept="video/*" className="invitation"  onChange={this.handleVideoUpload}/>
                    <img id="upload" src={Cloud} alt="upload"/>
                    </div>
                  : <div id="tick">
                  <img src={this.state.loadingInvitationVideo?Cloud:Tick} alt="Next" width="25px"/>
                  </div>
                  }
                </div>
              </div>
                <div>
                {
                  this.props.photo.invitationVideo !== undefined ?
                  <div id="close"><p className="">Invitation video</p>
                    <button onClick={(event)=>this.removeInvitationVideo(event,this.props.photo.invitationVideo)} id="closebtn">X</button></div>:null
                }
              </div>
              </div>
            }
          </div>


          <div className="col s7 Editor">
          <p className=" videoInfo">
            Hold and drag the video to position on the invite
          </p>
            <div className="white imageEditor">
              <img id="invitationImage" className="invitationImage" src={this.props.photo.invitationImage} alt=""/>
              {
                this.props.photo.invitationVideo === undefined
                  ? null
                  : <div id="container"><video id="item" width={videoWidth} controls="controls">
                      <source src={this.props.photo.invitationVideo} type="video/mp4"/>
                      Your browser does not support the video tag.
                    </video></div>
              }
            </div>
            {
                this.props.photo.invitationVideo === undefined
                  ? null
                  :<div className="col s11 sliderContainer">
                  <p className="col s3  resizeVideo">Resize Video:</p>
                    <p className="range-field col s7">
                      <input className="" type="range" id="test5" min="1" max="100" defaultValue={this.state.range} onInput={this.slider}/>
                    </p>
                  <p id="position"></p>
                  </div>
            }
            
          </div>
          {
          this.props.photo.invitationImage !== undefined&&this.props.photo.invitationVideo !== undefined?
          // <div onClick={this.handleNext} id="nextBtnInvitation">
          //     <img src={NextBtn} alt="Next" width="60px"/>
          //   </div>
          <div onClick={this.handleNext} id="nextBtnNew">
          <b>Next :</b> Publish & Preview
          </div>
            :null
          }
        </div>
    </div>)
  }
}

const mapStateToProps = (state) => {
  return {photo: state.photo}
}

const mapDispathToProps = (dispatch) => {
  return {
    addInvitationImage: (invitationImage,vuforia,extendedTracking,template) => {
      dispatch({
        type: "ADD_PHOTO_IMAGE",
        payload: {
          invitationImage,
          vuforia,
          extendedTracking,
          template
        }
      })
    },
    addInvitationVideo: (invitationVideo) => {
      dispatch({
        type: "ADD_PHOTO_VIDEO",
        payload: {
          invitationVideo
        }
      })
    },
    removeInvitationVideo: () => {
      dispatch({
        type: "REMOVE_PHOTO_VIDEO",
      })
    },
    addTargetWidth:(targetWidth)=>{
      dispatch({
        type: "ADD_TARGET_WIDTH_PHOTO",
        payload:{
          targetWidth
        }
      })
    },
    removeInvitationImage: () => {
      dispatch({
        type: "REMOVE_PHOTO_IMAGE",
      })
    },
    scaleAndPosition: (scale,position,width,height) => {
      dispatch({
        type: "SCALE_AND_POSITION_PHOTO",
        payload:{
          scale,
          position,
          width,
          height
        }
      })
    },
    resetPositionAndScale:()=>{
      dispatch({
        type: "RESET_POSITION_SCALE_PHOTO",
      })
    }
  }
}

export default connect(mapStateToProps, mapDispathToProps)(PhotoUpload)

// import React from 'react'
// import {connect} from 'react-redux'
// import {Modal} from 'react-materialize'
// import StyledFirebaseAuth from 'react-firebaseui/StyledFirebaseAuth';
// import firebase from 'firebase/app';
// import 'firebase/auth';
// import axios from 'axios'
// import { sendOtp, signup, login } from '../scripts/uri';
// import Loading from './loading';

// class Login extends React.Component {
//   constructor(props) {
//     super(props);
//     this.state = {
//       videourl:"",
//       upload:"",
//       otpCheck:"",
//       otp:false,
//       loading:true,
//       signup:false,
//       forget_pass:false,
//       reset_pass:false
//     }
//   }

//   uiConfig = {
//     signInFlow: 'popup',
//     signInOptions: [
//       firebase.auth.GoogleAuthProvider.PROVIDER_ID,
//       firebase.auth.FacebookAuthProvider.PROVIDER_ID,
//       // firebase.auth.EmailAuthProvider.PROVIDER_ID
//     ],
//     callbacks: {
//       signInSuccessWithAuthResult: () => false
//     }
//   };

//   componentDidMount() {
//     this.unregisterAuthObserver = firebase.auth().onAuthStateChanged(
//         (user) => {
//           if(user){
//           this.setState({isSignedIn: !!user,user:user,loading:false});
//           console.log(user)
//             this.props.saveUser({id:user.email, type:user})
//           }else{
//           this.setState({isSignedIn: false,loading:false});
//           }
//         }
//     );
//     const urlParams = new URLSearchParams(window.location.search);
//     const id = urlParams.get('id');
//     const email = urlParams.get('email');
//     if(id&&email)
//     {
//       this.setState({reset_pass:true})
//     }
//   }

//   componentWillUnmount() {
//     this.unregisterAuthObserver();
//   }

//   handleChange=(event)=>{
//     this.setState({[event.target.id]:event.target.value})
//   }

//   // handleChange = (event) => {
//   //   this.setState({
//   //     [event.target.id]:event.target.value
//   //   });
//   //   if(event.target.value.length===4){
//   //     this.otpCheck(event.target.value)
//   //   }
//   // }

//   // handleChange1 = (event) => {
//   //   this.setState({
//   //     [event.target.id]:event.target.value
//   //   });
//   // }

//   otpCheck = (otp) => {
//     console.log(this.props.auth,otp);
//     otp = parseInt(otp,10);
//     if(this.props.auth.otp===otp){
//       this.setState({
//         status: "success"
//       })
//     }
//   }

//   handleNext = (event) => {
//     event.preventDefault();
//     this.props.history.push('/payment');
//   }

//   // sendOTP = (event) => {
//   //   event.preventDefault();
//   //   if(this.state.phone.length===10)
//   //   {
//   //     var data = {
//   //       phone:this.state.phone
//   //     }
//   //     axios.post(sendOtp, data)
//   //     .then(response => {
//   //       if(response.data.status==='success'){
//   //         this.props.sendOTP(response.data.otp);
//   //       }
//   //     })
//   //     .catch(error => {
//   //       console.log(error);
//   //     })
//   //     this.setState({otpCheck:true})
//   //   }else{
//   //     alert("Enter Valid Phone Number")
//   //   }
//   // }

//   handleSignup=(event)=>{
//     event.preventDefault();
//     let body = {
//       email:this.state.email,
//       password:this.state.password
//     }
//     axios.post(signup,body).then(res=>{
//       if(res.data.status==="success"){
//         this.props.saveUser({id:res.data.data.email, type:{email:res.data.data.email}})
//         this.props.history.push('/product')
//       }
//     }).catch(error => {
//       console.log(error);
//     })
//   }

//   handleLogin=(event)=>{
//     event.preventDefault();
//     let body = {
//       email:this.state.email,
//       password:this.state.password
//     }
//     axios.post(login,body).then(res=>{
//       if(res.data.status === "success"){
//         this.props.saveUser({id:res.data.email, type:{email:res.data.email}})
//         this.props.history.push('/product')
//       }
//     }).catch(error => {
//       console.log(error);
//     })
//   }

//   Signup=(event)=>{
//     event.preventDefault();
//     this.setState({signup:true})
//   }

//   Login=(event)=>{
//     event.preventDefault();
//     this.setState({signup:false})
//   }


//   render () {
//     if(this.state.isSignedIn){
//       this.props.history.push('/product')
//     }
//     return(
//       <div className="parentContainer">
//       {this.state.loading?<Loading/>:null}
//         {/* {!this.props.auth.user?
//         <Modal
//             open={!this.state.isSignedIn?true:false}
//             className="modal"
//             actions={null}
//             > */}
//           <div className="loginContainer">

//           <div className="login">
//             {/* <div className="row">
//             {
//                 !this.state.otpCheck?
//               <div className="input-field col s12">
//                 <input id="phone" type="text" className="validate" onChange={this.handleChange1}/>
//                 <label htmlFor="phone">Phone</label>
//                 <button className="btn primary col s6" onClick={this.sendOTP}>Send OTP</button>
//               </div>
//               :null
//             }
//             {
//               this.state.otpCheck?
//                 <div className="input-field col s12">
//                   <input id="otp" type="text" className="validate" maxLength="4" autoComplete="false" onChange={this.handleChange}/>
//                   <label htmlFor="otp">Enter OTP</label>
//                   <p>{this.state.status}</p>
//                 </div>:null
//               }
//             </div> */}
//             {!this.state.forget_pass||!this.state.reset_pass?
//             this.state.signup===false?

//             <div className="loginWrap">
//             <h5>Login</h5>

//               <div className="input-field col s12">
//                   <input id="email" type="text" className="validate" onChange={this.handleChange}/>
//                   <label htmlFor="email">Email</label>
//               </div>
//               <div className="input-field col s12">
//                   <input id="password" type="password" className="validate" onChange={this.handleChange}/>
//                   <label htmlFor="password">Password</label>
//               </div>
//               <div className="loginBtnWrap col s12">
//                   <input id="loginBtn" type="submit" value="Login" className="validate" onClick={this.handleLogin}/>
//               </div>
//               <div className="loginBtnWrap col s12">
//                   <button id="loginBtn" type="submit" value="Signup" className="validate" onClick={this.Signup}>Signup</button>
//               </div>
//             </div>
//             :<div className="loginWrap">
//             <h5>Signup</h5>
//             <div className="input-field col s12">
//                 <input id="email" type="text" className="validate" onChange={this.handleChange}/>
//                 <label htmlFor="email">Email</label>
//             </div>
//             <div className="input-field col s12">
//                 <input id="password" type="password" className="validate" onChange={this.handleChange}/>
//                 <label htmlFor="password">Password</label>
//             </div>
//             <div className="loginBtnWrap col s12">
//                 <input id="loginBtn" type="submit" value="Signup" className="validate" onClick={this.handleSignup}/>
//             </div>
//             <div className="loginBtnWrap col s12">
//                   <button id="loginBtn" type="submit" value="Login" className="validate" onClick={this.Login}>Login</button>
//               </div>
//           </div>:this.state.forget_pass?
//           <div className="loginWrap">
//             <h5>Forgot Password</h5>
//             <div className="input-field col s12">
//                 <input id="email" type="text" className="validate" onChange={this.handleChange}/>
//                 <label htmlFor="email">Email</label>
//             </div>
//             <div className="loginBtnWrap col s12">
//                   <input id="loginBtn" type="submit" value="Send" className="validate" onClick={this.handleResetPass}/>
//             </div>
//             <div className="loginBtnWrap col s12">
//                 <button id="loginBtn" type="submit" value="Login" className="validate" onClick={this.Login}>Login</button>
//             </div>
//           </div>:this.state.reset_pass?
//           <div className="loginWrap">
//           <h5>Reset Password</h5>
//           <div className="input-field col s12">
//                   <input id="password" type="password" className="validate" onChange={this.handleChange}/>
//                   <label htmlFor="password">Password</label>
//             </div>
//             <div className="input-field col s12">
//                   <input id="confirm_password" type="password" className="validate" onChange={this.handleChange}/>
//                   <label htmlFor="password">Confirm Password</label>
//             </div>
//           <div className="loginBtnWrap col s12">
//                 <input id="loginBtn" type="submit" value="Send" className="validate" onClick={this.handleForgetPass}/>
//           </div>
//           <div className="loginBtnWrap col s12">
//                 <button id="loginBtn" type="submit" value="Login" className="validate" onClick={this.Login}>Login</button>
//           </div>
//         </div>:null}
//         </div>
//         <div className="loginSocialTitle">
//             <div className="hLine"></div>
//             <div className="">Social</div>
//             <div className="hLine"></div>
//         </div>
//           {!this.state.isSignedIn?
//             <div>
//               <StyledFirebaseAuth uiConfig={this.uiConfig} firebaseAuth={firebase.auth()}/>
//             </div>
//           :null
//           // <div>
//           //     <p>Welcome {firebase.auth().currentUser.displayName}! You are now signed-in!</p>
//           //   </div>
//           }
//         {/* </Modal>:null */}
//         </div>
//       </div>
//     )
//   }
// }

// const mapStateToProps = (state) =>{
//   return {
//     auth:state.auth
//   }
// }

// const mapDispathToProps = (dispatch) => {
//   return {
//     sendOTP: (otp) => {dispatch({type:"SEND_OTP",payload:otp});},
//     saveUser: (user) => {dispatch({type:"SAVE_USER",payload:user})},
//   }
// }



// export default connect(mapStateToProps,mapDispathToProps)(Login)
