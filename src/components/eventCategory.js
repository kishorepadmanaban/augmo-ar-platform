import React from 'react'
import {connect} from 'react-redux'
import WeddingIcon from '../assets/ui/Wedding Icon.svg'
import OtherIcon from '../assets/ui/Event Icon.svg'
import ProcessStepper from './processStepper';

class eventCategory extends React.Component {
  constructor(props) {
    super(props);
    this.state = {

    }
  }

  render () {
    return(
    <div>
        <ProcessStepper pathname={0}/>
        <h4 className="center ">Select Your Category</h4>
        <div className="row container"  id="containerCategory">
            <div id="cardWrap">
                <div onClick={()=>{this.props.addEventCategory("wedding");this.props.history.push('/name');}} id="card" className="col s3 card">
                    <img id="weddingIcon" src={WeddingIcon} alt="Wedding"/>
                </div>
                <div id="cardText">
                  <h5 className="center ">Wedding</h5>
                </div>
            </div>

            <div id="cardWrap">
                <div onClick={()=>{this.props.addEventCategory("others");this.props.history.push('/name');}} id="card" className="col s3 card">
                    <img id="otherIcon" src={OtherIcon} alt="Other events"/>
                </div>
                <div id="cardText">
                  <h5 className="center ">Other Events</h5>
                </div>
            </div>

        </div>
    </div>

    )
  }
}


const mapStateToProps = (state) => {
  return {project: state.project}
}

const mapDispathToProps = (dispatch) => {
  return {
    addEventCategory: (eventCategory) => {dispatch({type:"ADD_EVENT_CATEGORY", payload:{eventCategory}})}
  }
}


export default connect(mapStateToProps,mapDispathToProps)(eventCategory)
