import React from 'react'
import {connect} from 'react-redux'
import PriceBackground from '../assets/ui/PriceBackground.svg'
import ProcessStepper from './processStepperPhoto';


class PhotoPrice extends React.Component {
  constructor(props) {
    super(props);
    this.state = {

    }
  }

  render () {
    return(
        <div>
        <ProcessStepper pathname={0}/>
            <h4 className="center ">Select Your Plan</h4>
            <div className="row container" id="containerCategory">
                <div id="priceCard" className="col s3 card" onClick={()=>{this.props.addPhotoPrice({plan:"basic",price:190});this.props.history.push('/photo_editor');}}>
                    <img id="priceBackground" src={PriceBackground} alt="price"/>
                    <div id="discount"><div id="discountPrice">50%</div><div id="discountOff">offer</div></div>
                    <div id="priceContent">
                        <h5 className="center white-text plan">BASIC</h5>
                        <div className="center white-text mrp"><sup id="mrpsup">₹</sup>300</div>
                        <h3 className="center white-text price"><sup id="inr">₹</sup>190</h3>
                        <h5 className="center white-text planTitle"><sup>____</sup> PLAN <sup>____</sup></h5>
                        <p className="center white-text features">1 Image</p>
                        <p className="center white-text features">Validity 1 year</p>
                        <p className="center white-text features">150 per photo</p>
                    </div>
                </div>
                <div id="priceCardPremium"  className="col s3 card" onClick={()=>{this.props.addPhotoPrice({plan:"premium",price:490});this.props.history.push('/photo_editor');}}>
                    <img id="priceBackground" src={PriceBackground} alt="price"/>
                    <div id="priceContent">
                        <h5 className="center white-text planPremium">PREMIUM</h5>
                        <div className="center white-text mrp"><sup id="mrpsup">₹</sup>750</div>
                        <h2 className="center white-text price"><sup id="inr">₹</sup>490</h2>
                        <h5 className="center white-text planTitle"><sup>____</sup> PLAN <sup>____</sup></h5>
                        <p className="center white-text features">3 Images</p>
                        <p className="center white-text features">Validity 6 months*</p>
                        <p className="center white-text features">120 per photo</p>
                    </div>
                </div>
                <div id="priceCard"  className="col s3 card" onClick={()=>{this.props.addPhotoPrice({plan:"elite",price:1190});this.props.history.push('/photo_editor');}}>
                <img id="priceBackground" src={PriceBackground} alt="price"/>
                    <div id="priceContent">
                        <h5 className="center white-text plan">ELITE</h5>
                        <div className="center white-text mrp"><sup id="mrpsup"></sup>1500</div>
                        <h2 className="center white-text price"><sup id="inr">₹</sup>1190</h2>
                        <h5 className="center white-text planTitle"><sup>____</sup> PLAN <sup>____</sup></h5>
                        <p className="center white-text features">10 Images</p>
                        <p className="center white-text features">100 per photo</p>
                    </div>
                </div>
            </div>
        </div>
)
  }
}


const mapStateToProps = (state) => {
  return {project: state.project}
}

const mapDispathToProps = (dispatch) => {
  return {
    addPhotoPrice: (photoPrice) => {dispatch({type:"ADD_PHOTO_PRICE", payload:{photoPrice}})}
  }
}


export default connect(mapStateToProps,mapDispathToProps)(PhotoPrice)
