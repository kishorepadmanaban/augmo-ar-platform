import React from 'react'

const Loading = () =>{
    return(
    <div style={{height:window.screen.height}} className="loading">
        {/* <h3>Loading</h3> */}
      <div className="loader">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
      </div>
    </div>
    )
}

export default Loading

// import React from 'react'

// const Loading = () =>{
//     return(
//     <div style={{height:window.screen.height}} className="loading">
//       <div className="body">
//           <span>
//             <span></span>
//             <span></span>
//             <span></span>
//             <span></span>
//           </span>
//           <div className="base">
//             <span></span>
//             <div className="face"></div>
//           </div>
//         </div>
//         <div className="longfazers">
//           <span></span>
//           <span></span>
//           <span></span>
//           <span></span>
//         </div>
//         <h1 className="h1">Loading</h1>
//       </div>
//     )
// }

// export default Loading