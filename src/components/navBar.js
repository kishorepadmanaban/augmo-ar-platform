import React from 'react'
import {connect} from 'react-redux'
import '../style/invitation.css'
import LoggedIn from './loggedIn';
import LoggedOut from './loggedOut';
import Login from './login'
import firebase from 'firebase/app';
import 'firebase/auth';



class NavBar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      login:false
    }
  }

  logout = () =>{
    firebase.auth().signOut();
    this.props.removeUser();
    this.setState({login:false})
    this.props.history.push('/');
    }

  login = ()=>{
    this.setState({login:true});
  }

  render () {
    return(
      <div>
        {this.props.auth.user?
        <LoggedIn logout={this.logout}/>:
        <div>
          {
           this.state.login? 
        <Login/>:null}
        <LoggedOut login={this.login}/>
        </div>
        }
      </div>
    )
  }
}

const mapStateToProps = (state) =>{
  return {
    auth:state.auth
  }
}

const mapDispathToProps = (dispatch) => {
  return {
    removeUser: () => {dispatch({type:"REMOVE_USER"})}
  }
}

export default connect(mapStateToProps,mapDispathToProps)(NavBar)



      // <div id="navbarContainer">
      //     <div>
      //       <NavLink className="navlink" activeStyle={css.active} to="/product">Product</NavLink>
      //       <NavLink className="navlink" activeStyle={css.active} to="/price">Price</NavLink>
      //       <NavLink className="navlink" activeStyle={css.active} to="/event">Event</NavLink>
      //       <NavLink className="navlink" exact activeStyle={css.active} to="/name">Name</NavLink>
      //       <NavLink className="navlink" activeStyle={css.active} to="/date">Date Location</NavLink>
      //       <NavLink className="navlink" activeStyle={css.active} to="/invitation">Invitation</NavLink>
      //       <NavLink className="navlink" activeStyle={css.active} to="/gallery">Gallery</NavLink>
      //       <NavLink className="navlink" activeStyle={css.active} to="/template">Template</NavLink>
      //       <NavLink className="navlink" activeStyle={css.active} to="/login">Login</NavLink>
      //       <NavLink className="navlink" activeStyle={css.active} to="/preview">Preview</NavLink>
      //       <NavLink className="navlink" activeStyle={css.active} to="/payment">Payment</NavLink>
      //     </div>
      // </div>