import React from 'react'
import {connect} from 'react-redux'
import {galleryVideo} from '../scripts/uri'
import { ToastContainer, toast, Slide } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';


class GalleryVideoUpload extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      videourl:"",
      upload:""
    }
  }

  handleChange = (event) => {
    this.setState({
      [event.target.id]:event.target.value
    });
  }

  handleNext = (event) => {
    event.preventDefault();

    this.props.addGalleryVideo({gallery_video_url:this.state.videourl});
    this.props.history.push('/template');

  }

  handleUpload = (event) => {
    event.preventDefault();
    const data = new FormData();
    //data.append('sampleFile',event.target.files[0] );
    data.append('galleryVideo', event.target.files[0]);
    /*for (var pair of data.entries()) {
    console.log(pair[0]+ ', ' + pair[1]);*/

    fetch(galleryVideo, {
        method: 'POST',
        body: data
      }).then((response) => response.json())
      .then((responseJson) => {
        console.log(responseJson.url)
        console.log(responseJson.status)
        if (responseJson.status === 'success') {
          this.setState({
            videourl: responseJson.url,
            upload: responseJson.status
          })
          toast.info("File Uploaded");
        }
      })
  }


  render () {
    return(
      <div className="row container">
        <form className="col s12">
          <div className="row">
            <div className="input-field col s12">
              <h3>Upload Gallery Video</h3>
              <input id="gallery_video" type="file" accept="video/*" className="gallery_video" ref='gallery_video' onChange={this.handleUpload}/>
              <h4>{this.state.upload}</h4>
              {this.state.upload==='success'?
                <video id="myVideo" width="320" height="240" controls>
                  <source src={this.state.videourl} type="video/mp4"/>
                  Your browser does not support the video tag.
              </video>:null}
            </div>
          </div>
          <button className="btn primary right" onClick={this.handleNext}>Next</button>
        </form>
        <ToastContainer position="bottom-right" transition={Slide} autoClose= {3000} hideProgressBar= {true}/>
      </div>
    )
  }
}

const mapDispathToProps = (dispatch) => {
  return {
    addGalleryVideo: (galleryVideoUrl) => {dispatch({type:"ADD_GALLERY_VIDEO", payload:galleryVideoUrl})}
  }
}


export default connect(null,mapDispathToProps)(GalleryVideoUpload)
