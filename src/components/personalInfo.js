import React from 'react'
import {Input,Row} from 'react-materialize'
import {connect} from 'react-redux'
import axios from 'axios';
import { personalDetails } from '../scripts/uri';

class PersonalInfo extends React.Component {
    constructor(props) {
    super(props);
      this.state = {
          data:this.props.auth.user?this.props.auth.user:{}
      }
    }
    
  handleInput = (event) =>{
    this.setState({data:{...this.state.data,[event.target.id]:event.target.value}})
  }

  handleSubmit = () =>{
    axios.post(personalDetails,this.state.data).then(res=>{
      this.props.personalDetails(this.state.data)
      console.log(res.data)
    })
  }

  render () {
    console.log(this.state.data)
    return(
      <div className="flexCenterColumn padding30">
        <div className="h3 bold">Personal Information</div>
          <Row>
                <Input s={12} id="name" label="Name" type="text" value={this.state.data.name} onChange={this.handleInput}/>
                <Input s={12} id="id" label="Email" type="text" value={this.state.data.id}/>
                <Input s={12} id="phone" label="Phone" type="text" value={this.state.data.phone} onChange={this.handleInput}/>
            </Row>
            <div id="saveAddress" onClick={this.handleSubmit}>
                  Submit
            </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
      project: state.project,
      auth:state.auth,
      photo:state.photo
  }
}

const mapDispathToProps = (dispatch) => {
  return {
    personalDetails: (details) => {
      dispatch({
        type: "PERSONAL_DETAILS",
        payload: details
      })
    },
  }
}

export default connect(mapStateToProps, mapDispathToProps)(PersonalInfo)
