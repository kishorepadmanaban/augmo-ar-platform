import React from 'react'
import {connect} from 'react-redux'
import {Row,Input} from 'react-materialize'
import WeddingVenue from '../assets/ui/Wedding Venue.svg'
import moment from 'moment'
import ProcessStepper from './processStepper';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';

class DateLocation extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      date:this.props.project.date?this.props.project.date:null,
      time:this.props.project.time,
      location:this.props.project.location?this.props.project.location.replace("https://www.google.com/maps/search/?api=1&query=", ''):'',
      rsvp:this.props.project.rsvp?this.props.project.rsvp:false
    }
  }

  componentDidMount(){

  }

  handleChange = (event) => {
    this.setState({
      [event.target.id]:event.target.value
    });
  }

  handleNext = (event) => {
    event.preventDefault();
    if(!this.state.date||!this.state.time||!this.state.location)
    {
      alert("Please fill in all the fields for the next step");
    }else{
    var time = this.state.time;
    var startTime = new Date(this.state.date);
    var parts = time.match(/(\d+):(\d+)(AM|PM)/);
    if (parts) {
        var hours = parseInt(parts[1]),
            minutes = parseInt(parts[2]),
            tt = parts[3];
        if (tt === 'PM' && hours < 12) hours += 12;
        startTime.setHours(hours, minutes, 0, 0);
    }
    var datetime = moment(startTime).format("DD/MM/YYYY HH:mm:ss")

    console.log(datetime)
    let dateLocation = {
      date: this.state.date,
      time: this.state.time,
      datetime:datetime,
      location: this.state.location,
      email:this.props.auth.user.id,
      rsvp:this.state.rsvp
    }

      dateLocation.location = "https://www.google.com/maps/search/?api=1&query="+this.state.location;
      this.props.addDateTimeLocation(dateLocation);
      this.props.history.push('/invitation');
    }
  }

  handleRSVP=(event)=>{
    this.setState({rsvp:!this.state.rsvp})
  }



  render () {
    return(
      <div>
        <ProcessStepper pathname={1}/>
        <form className="row container">
          <div className="row">
          <div className="col s12 m6" id="groomBrideIconContainer">
            <h5 className="center ">Enter Your Details</h5>
            <div id="card" className="col s3">
                <img id="groomAndBride" src={WeddingVenue} alt="groomAndBride"/>
            </div>
          </div>
          <div className="col s12 m6" id="groomBrideInputContainer">
              <Input s={12} id="location" type="text" labelClassName="" label="Location" required defaultValue={this.state.location} onChange={this.handleChange}/>
              <div id="locationLinkWrap">
                <span id="locationLinkText">{this.state.location?"Check if your location is correct! -":null}</span><a id="locationLink" target="_blank" rel="noopener noreferrer" href={"https://www.google.com/maps/search/?api=1&query="+this.state.location}>{this.state.location?"Click here":null}</a>
              </div>
              <Row id="DateRow">
                <Input label="Date" required placeholder={this.state.date} id="date" labelClassName="" name='on' type='date' min={moment().format("YYYY-MM-DD")} onChange={(e, value)=> {this.setState({date:value})}}/>
                <Input label="Time" id="time" value={this.state.time} labelClassName="" name='on' type='time' onChange={(e, value)=> {this.setState({time:value})}}/>
              </Row>
              <div id="checkbox">
              <Row id="DateRow">
              <FormControlLabel control={<Checkbox checked={this.state.rsvp} onChange={this.handleRSVP}/>} label="RSVP" />
                {/* <Input name='rsvp' type='checkbox' defaultChecked={this.state.rsvp?'checked':null} label='RSVP' onChange={this.handleRSVP}/> */}
              </Row>
              </div>
          </div>
          <div onClick={this.handleNext} id="nextBtnNew">
          <b>Next :</b> Upload Invitation
          </div>
          </div>
        </form>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    project: state.project,
    auth:state.auth}
}

const mapDispathToProps = (dispatch) => {
  return {
    addDateTimeLocation: (dateLocation) => {dispatch({type:"ADD_DATETIME_LOCATION", payload:dateLocation})}
  }
}


export default connect(mapStateToProps,mapDispathToProps)(DateLocation)
