import React from 'react'
import {NavLink} from 'react-router-dom'
import '../style/invitation.css'
import Logo from "../assets/images/augmoLogo.svg"
import axios from 'axios'
import { deleteAllToLogout } from '../scripts/uri';
import {connect} from 'react-redux'

class LogginIn extends React.Component {
constructor(props) {
    super(props);
    this.state = {
    }
    }

  logout=()=>{
    if(window.confirm("Logging out will delete all unsaved data")===true){
      let body = {
        project:this.props.project,
        photo:this.props.photo,
        auth:this.props.auth,
      }
      axios.post(deleteAllToLogout,body).then(res=>{
        this.props.logout();
        this.props.clearData();
      }).catch(error=>{
        alert(error)
      })
    }
  }
    
  render () {
    return(
      <div id="navbarContainer" className="flexCenter">
      <div className="container">
        <div className="col s5 flexStart">
            <img className="logo" src={Logo} alt="logo" width="120px"/>
          </div>
          <div className="col s7 flexEnd">
            <NavLink className=" h4 navitem" to="my_orders">
                My Orders
            </NavLink>
            <div className="navitem h4" onClick={this.logout}>
                Logout
            </div>
        </div>
      </div>

      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    project: state.project,
    photo: state.photo,
    auth: state.auth,
  }
}

const mapDispathToProps = (dispatch) => {
  return {
    clearData: () => {dispatch({type:"CLEAR_DATA"})}
  }
}

export default connect(mapStateToProps, mapDispathToProps)(LogginIn)