import React from 'react'
import {connect} from 'react-redux'
import {Link} from 'react-router-dom'
import {addInvitationImage, addInvitationVideo, removeInvitationImage, removeInvitationVideo, sendMetadataPhoto, deleteTargetFromDatabase} from '../scripts/uri'
import axios from 'axios'
import StarRatings from 'react-star-ratings';
import Tick from '../assets/ui/tick.svg'
import Cloud from '../assets/ui/Cloud.svg'
import Error from '../assets/ui/error.svg'
import ProcessStepper from './processStepperPhoto';
import Loading from './loading';

class PhotoUpload extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      upload: "",
      progress: 0,
      targetWidth: this.props.photo.photos[0]?this.props.photo.photos[0].targetWidth:100,
      range:this.props.photo.photos[0]?this.props.photo.photos[0].scale:100,
      value:5,
      position:this.props.photo.position?this.props.photo.position:[0,0],
      invitationWidth:null,
      invitationHeight:null,
      index:0
    }
  }

  componentDidMount(){
    if(this.props.photo.photos[this.state.index])
    {
      if(this.props.photo.photos[this.state.index].invitationVideo && this.props.photo.photos[this.state.index].position){
        var dragItem = document.querySelector("#item");
        dragItem.style.transform = "translate3d(" + this.props.photo.photos[this.state.index].position[0] + "px, " + (-this.props.photo.photos[this.state.index].position[1]) + "px, 0)";
      }
    }
    if(!this.props.photo.email)
    {
      this.props.addEmail(this.props.auth.user.id)
    }

      this.dragElement();
  }


  slider = () => {
    var slider = document.getElementById("test5");
    this.setState({range:slider.value})
    this.props.scaleAndPosition(parseInt(slider.value,10),this.state.position,this.props.photo.photos[this.state.index].width,this.props.photo.photos[this.state.index].height,this.state.index);

  }



  dragElement = () => {
    var dragItem = document.querySelector("#item");
    var container = document.querySelector("#container");
    var position = document.querySelector("#position");
    var invitationImage = document.querySelector("#invitationImage");
    if(container)
    {

    var active = false;
    var currentX;
    var currentY;
    var initialX;
    var initialY;
    var xOffset = this.props.photo.position?this.props.photo.position[0]:0;
    var yOffset = this.props.photo.position?-this.props.photo.position[1]:0;

    container.addEventListener("touchstart", dragStart, false);
    container.addEventListener("touchend", dragEnd, false);
    container.addEventListener("touchmove", drag, false);
  
    container.addEventListener("mousedown", dragStart, false);
    container.addEventListener("mouseup", dragEnd, false);
    container.addEventListener("mousemove", drag, false);
    container.addEventListener("mouseout", dragEnd, false);

    invitationImage.addEventListener("touchstart", dragStart, false);
    invitationImage.addEventListener("touchend", dragEnd, false);
    invitationImage.addEventListener("touchmove", drag, false);
  
    invitationImage.addEventListener("mousedown", dragStart, false);
    invitationImage.addEventListener("mouseup", dragEnd, false);
    invitationImage.addEventListener("mousemove", drag, false);
  
    function dragStart(e){
      if (e.type === "touchstart") {
        initialX = e.touches[0].clientX - xOffset;
        initialY = e.touches[0].clientY - yOffset;
      } else {
        initialX = e.clientX - xOffset;
        initialY = e.clientY - yOffset;
      }
  
      if (e.target === dragItem) {
        active = true;
      }
    }
  
    function dragEnd(e) {
      initialX = currentX;
      initialY = currentY;
      active = false;
      setProps()
    }
  
    function drag(e) {
      if (active) {
      
        e.preventDefault();
      
        if (e.type === "touchmove") {
          currentX = e.touches[0].clientX - initialX;
          currentY = e.touches[0].clientY - initialY;
        } else {
          currentX = e.clientX - initialX;
          currentY = e.clientY - initialY;
        }
  
        xOffset = currentX;
        yOffset = currentY;
  
        setTranslate(currentX, currentY, dragItem);
      }
    }

    var setProps=()=>{
      this.props.scaleAndPosition(parseInt(this.state.range,10),this.state.position,this.props.photo.photos[this.state.index].width,this.props.photo.photos[this.state.index].height,this.state.index);
    }

    var setTranslate=(xPos, yPos, el)=> {
      el.style.transform = "translate3d(" + xPos + "px, " + yPos + "px, 0)";
      position.innerHTML = xPos+","+yPos;
      this.setState({position:[xPos,-yPos]})
    }
  }

  }



  handleNext = (event) => {
    event.preventDefault();
    const data = {
      photos:this.props.photo,
      index:this.state.index,
      metadata:this.props.photo.photos[this.state.index],
      target_id:this.props.photo.photos[this.state.index].vuforia.target_record.target_id
    }
    this.setState({loading:true})
    axios.post(sendMetadataPhoto, data).then(response => {
      console.log(response.data);
      if (response.data.status === 'success')
      {
        this.setState({loading:false})
        this.props.published(this.state.index,response.data._id)
        alert("Photo Successfully Published")
      }
    }).catch(error => {
      console.log(error);
    })  
  }

  checkImage=(event)=>{
    event.persist();
    var reader = new FileReader();
    //Read the contents of Image File.
    reader.readAsDataURL(event.target.files[0]);
    reader.onloadend = (e) =>{
    var image = new Image();
    image.src = e.currentTarget.result;
    
    image.onload = e =>{
      //Determine the Height and Width.
      var height = e.currentTarget.width;
      var width = e.currentTarget.width;
      console.log(height,width)
      if (height < 400 && width < 400) {
          alert("Height or Width should be greater than 400px.");
          }else{
            this.handleImageUpload(event);
          }
        }
      };
  }

   handleImageUpload = (event)=> {
    if(event.target.files[0].size>2097152)
    {
      alert("File size exceeded - Please upload image less than 2MB in size.")
    } else{

    this.setState({loadingInvitation:true})
    const data = new FormData();
    data.append('invitationImage', event.target.files[0]);
    console.log(event.target.files[0])
    const config = {
      onUploadProgress: progressEvent => {
        let progress = (progressEvent.loaded * 66) / progressEvent.total
        this.setState({progress: progress});
      }
    }
    this.setState({loading:true})
    axios.post(addInvitationImage, data, config).then(response => {
      console.log(response.data);
      if(response.data.status==="success")
      {
        this.setState({loading:false})
        this.setState({progress: 100,loadingInvitation:false});
        let extendedTracking = true;
        let template = ["default","http://augmo.net/src/invitationdefault","http://augmo.net/src/invitationdefault"];
        let productCategory = this.props.photo.productCategory;
        let photoPrice      = this.props.photo.photoPrice;
        this.props.addInvitationImage(response.data.image,response.data.response,extendedTracking,template,productCategory,photoPrice,this.state.index);

      }else if(response.data.error==="Bad Image"){
        this.setState({loading:false})

        alert("Bad Image - Please upload some other image since this image is not suitable for augmentation")

      }else if(response.data.error==="Duplicate target exist"){
        this.setState({loading:false})

        alert("Duplicate image exist - Please upload any other image since this image is already exists in our database")

      }
    }).catch(error => {
      console.log(error);
    })
  }
}

  handleVideoUpload = (event) => {
    this.setState({loadingInvitationVideo:true})

    const data = new FormData();
    data.append('invitationVideo', event.target.files[0]);

    const config = {
      onUploadProgress: progressEvent => {
        let progress = (progressEvent.loaded * 83) / progressEvent.total
        this.setState({
          progress: progress
        });
      }
    }
    this.setState({loading:true})

    axios.post(addInvitationVideo, data, config).then(response => {
      this.setState({loading:false})
      var img = document.getElementById("invitationImage");
      this.setState({
        progress: 100,
        loadingInvitationVideo:false,
        status: response.data.status,
        targetWidth: img.clientWidth,
        range:100
      });
      this.props.scaleAndPosition(100,this.state.position,img.naturalWidth,img.naturalHeight,this.state.index);
      this.props.addInvitationVideo(response.data.url,this.state.index);
      this.props.addTargetWidth(img.clientWidth,this.state.index);
      this.dragElement();

    }).catch(error => {
      console.log(error);
    })
  }

  removeInvitationVideo = (video) =>{
    let videoPath = video.replace("http://159.65.146.12/invitation/", '')
    const data = {
      image:videoPath
    }

    this.setState({loading:true})
    axios.delete(removeInvitationVideo, {data}).then(response => {
      if (response.data.status === 'success')
      {
        this.setState({loading:false})
        this.setState({progress: 100,position:[0,0],scale:100});
        this.props.removeInvitationVideo(this.state.index);
        this.props.resetPositionAndScale(this.state.index);
        return true;
      }
    }).catch(error => {
      console.log(error);
    })
  }


  removeInvitationImage = () =>{
    this.setState({loading:true})
    axios.delete(removeInvitationImage+this.props.photo.photos[this.state.index].vuforia.target_record.target_id).then(response => {
      this.setState({loading:false})
      if(response.data.result_code==="Success")
      {
        this.props.removeInvitationImage(this.state.index);
        return true;
      }
    }).catch(error => {
      console.log(error);
    })
  }

  deletePhoto=(index)=>{
    if(this.props.photo.photos.length===1)
    {
      alert("You cannot delete all component")
    }else{

    if(this.props.photo.photos[index])
    {
      if(this.props.photo.photos[index].invitationVideo)
      {
        let videoPath = this.props.photo.photos[index].invitationVideo.replace("http://159.65.146.12/invitation/", '')
        const data = {
          image:videoPath
        }
    
        axios.delete(removeInvitationVideo, {data}).then(response => {
          if (response.data.status === 'success')
          {
            this.setState({position:[0,0],scale:100});
            this.props.removeInvitationVideo(index);
            this.props.resetPositionAndScale(index);
            return true;
          }
        }).catch(error => {
          console.log(error);
        })
        // this.removeInvitationVideo(this.props.photo.photos[index].invitationVideo)
      }
    }
    this.setState({loading:true})

    if(this.props.photo.photos[index])
    {
      if(this.props.photo.photos[index].invitationImage)
      {
        axios.delete(removeInvitationImage+this.props.photo.photos[index].vuforia.target_record.target_id).then(response => {
          if(response.data.result_code==="Success")
          {
            this.setState({index:0,loading:false})
            this.setState({range:this.props.photo.photos[index].scale,targetWidth:this.props.photo.photos[index].targetWidth})
            this.props.deletePhoto(index)
          }
        }).catch(error => {
          console.log(error);
        })
      }else{
        this.setState({index:0,loading:false})
        this.setState({range:this.props.photo.photos[index].scale,targetWidth:this.props.photo.photos[index].targetWidth})

        this.props.deletePhoto(index)
      }
    }
  }
  }

  handleIndex=(index)=>{
    this.setState({index})
    
    setTimeout(()=>{
      var dragItem = document.querySelector("#item");
    if(this.props.photo.photos[index].invitationVideo && this.props.photo.photos[index].position && dragItem){
      dragItem.style.transform = "translate3d(" + this.props.photo.photos[index].position[0] + "px, " + (-this.props.photo.photos[index].position[1]) + "px, 0)";
    }
    }, 1000);
    this.setState({range:this.props.photo.photos[index].scale,targetWidth:this.props.photo.photos[index].targetWidth})
    this.dragElement();

  }

  addPhoto=()=>{
    this.props.addPhoto();
    // this.setState({index:this.props.photo.photos.length})
  }



  render() {
    if(!this.props.photo.photos[0])
    {
      console.log("test")
      this.props.addFirstObject();
    }
    if(this.state.loading){
      document.getElementsByTagName("BODY")[0].style.overflow = "hidden";
    }else{
      document.getElementsByTagName("BODY")[0].style.overflow = "auto";
    }
    let {index} = this.state

    let videoWidth = (this.state.targetWidth*(this.state.range/100)).toString()+"px"
    return (<div>
      {this.state.loading?<Loading/>:null}
        <ProcessStepper pathname={1}/>
        <div className="row">
          <div className="photo-list col s2 m1">
            {this.props.photo.photos?Object.values(this.props.photo.photos).map((value,index)=>
              <div key={index} style={{background:index===this.state.index?"linear-gradient(to right, #FF4B2B, #FF416C)":null, color:index===this.state.index?"white":null}} className="photo-list-item">
                <div onClick={()=>this.deletePhoto(index)} className="close-btn"><img src={Error} alt="close" width="20px"/></div>
                {value.invitationImage?
                <img className="photo-list-img" onClick={()=>this.handleIndex(index)} src={value?value.invitationImage:null} alt="thumb" width="50px"/>:
                <div onClick={()=>this.handleIndex(index)} className="photo-list-noimg">
                  {index+1}
                </div>
                }
              </div>
              ):null}
              <div className="add-photo-list" onClick={this.addPhoto}>
                Add
              </div>
          </div>
          <div className="input-field offset-m1 col s12 m4">
            <h4>Upload Details</h4>
            <div>
            <div id="invitationWrap">
              <div>
                <h5>Upload Photo</h5>
              </div>
              <div>
                {
                  this.props.photo.photos[index]?this.props.photo.photos[index].invitationImage === undefined
                  ?<div id="invitationDiv"><input id="invitation" type="file" accept="image/*" className="invitation" ref='invitation' onChange={this.checkImage}/>
                    <img id="upload" src={Cloud} alt="upload"/>
                  </div>:
                  <div id="tick">
                    <img src={Tick} alt="Next" width="25px"/>
                  </div>:null
                }
              </div>
            </div>
              <div>
                {
                this.props.photo.photos[index]?this.props.photo.photos[index].invitationImage !== undefined
                ?
                 <div id="close"><p className="">Photo</p>
              <button onClick={(event)=>this.removeInvitationImage(event)} id="closebtn">X</button></div>:null:null
                }
                {
                  this.props.photo.photos[index]?this.props.photo.photos[index].vuforia === undefined
                    ? null
                    : <div className="rating">
                        <StarRatings starEmptyColor="rgba(203, 211, 227, 0.3)" rating={this.props.photo.photos[index].vuforia.target_record.tracking_rating} numberOfStars={5} starDimension="20px" starRatedColor="#FF416C" starSpacing="3px"/></div>:null
                }
              </div>
            </div>

            {
              this.props.photo.photos[index]?this.props.photo.photos[index].invitationImage === undefined?null:
              <div>
                <div id="invitationVideoWrap">
                  <div>
                  <h5>Upload Video</h5>
                  </div>
                  <div>
                  {
                    this.props.photo.photos[index].invitationVideo === undefined
                    ? <div id="invitationDiv"><input id="invitation" type="file" accept="video/*" className="invitation"  onChange={this.handleVideoUpload}/>
                    <img id="upload" src={Cloud} alt="upload"/>
                    </div>
                  : <div id="tick">
                  <img src={this.state.loadingInvitationVideo?Cloud:Tick} alt="Next" width="25px"/>
                  </div>
                  }
                </div>
              </div>
                <div>
                {
                  this.props.photo.photos[index].invitationVideo !== undefined ?
                  <div id="close"><p className="">Video</p>
                    <button onClick={(event)=>this.removeInvitationVideo(this.props.photo.photos[index].invitationVideo)} id="closebtn">X</button></div>:null
                }
              </div>
              </div>:null
            }
          </div>


          <div className="col s12 m5 Editor">
          <p className=" videoInfo">
            Hold and drag the video to position on the invite
          </p>
            <div className="white imageEditor">
              <img id="invitationImage" className="invitationImage" src={this.props.photo.photos[index]?this.props.photo.photos[index].invitationImage:null} alt=""/>
              {
                this.props.photo.photos[index]?this.props.photo.photos[index].invitationVideo === undefined
                  ? null
                  : <div id="container"><video id="item" src={this.props.photo.photos[index]?this.props.photo.photos[index].invitationVideo:null}  type="video/mp4" width={videoWidth} controls="controls">
                    </video></div>:null
              }
            </div>
            {
                this.props.photo.photos[index]?this.props.photo.photos[index].invitationVideo === undefined
                  ? null
                  :<div className="col s11 sliderContainer">
                  <p className="col s3  resizeVideo">Resize Video:</p>
                    <p className="range-field col s7">
                      <input className="" type="range" id="test5" min="1" max="100" value={this.state.range} onInput={this.slider}/>
                    </p>
                  <p id="position"></p>
                  </div>:null
            }
            {
              this.props.photo.photos[index]?this.props.photo.photos[index].invitationImage !== undefined&&this.props.photo.photos[index]?this.props.photo.photos[index].invitationVideo !== undefined?
              <div id="nextBtnNewDiv">
              <div onClick={this.handleNext} id="nextBtnNew">
              Publish
              </div>
              <Link to={{
                pathname: '/preview_photo',
                state: { productCategory: "photo" }
              }} id="nextBtnNew">
              <b>Next :</b> Preview
              </Link>
              </div>
                :null:null:null
            }
            
          </div>

        </div>
    </div>)
  }
}

const mapStateToProps = (state) => {
  return {
    photo: state.photo,
    auth:state.auth
  }
}

const mapDispathToProps = (dispatch) => {
  return {
    addInvitationImage: (invitationImage,vuforia,extendedTracking,template,productCategory,photoPrice,index) => {
      dispatch({
        type: "ADD_PHOTO_IMAGE",
        payload: {
          invitationImage,
          vuforia,
          extendedTracking,
          template,
          productCategory,
          photoPrice,
          index
        }
      })
    },
    addInvitationVideo: (invitationVideo,index) => {
      dispatch({
        type: "ADD_PHOTO_VIDEO",
        payload: {
          invitationVideo,
          index
        }
      })
    },
    removeInvitationVideo: (index) => {
      dispatch({
        type: "REMOVE_PHOTO_VIDEO",
        payload:{
          index
        }
      })
    },
    addTargetWidth:(targetWidth,index)=>{
      dispatch({
        type: "ADD_TARGET_WIDTH_PHOTO",
        payload:{
          targetWidth,
          index
        }
      })
    },
    removeInvitationImage: (index) => {
      dispatch({
        type: "REMOVE_PHOTO_IMAGE",
        payload:{
          index
        }
      })
    },
    scaleAndPosition: (scale,position,width,height,index) => {
      dispatch({
        type: "SCALE_AND_POSITION_PHOTO",
        payload:{
          scale,
          position,
          width,
          height,
          index
        }
      })
    },
    resetPositionAndScale:(index)=>{
      dispatch({
        type: "RESET_POSITION_SCALE_PHOTO",
        payload:{
          index
        }
      })
    },
    addPhoto:()=>{
      dispatch({
        type:"ADD_PHOTO"
      })
    },
    deletePhoto:(index)=>{
      dispatch({
        type:"DELETE_PHOTO",
        payload:{
          index
        }
      })
    },
    addFirstObject:()=>{
      dispatch({
        type:"ADD_FIRST_OBJECT"
      })
    },
    published:(index,_id)=>{
      dispatch({
        type:"PUBLISHED",
        payload:{
          index,
          _id
        }
      })
    },
    addEmail:(email)=>{
      dispatch({
        type:"ADD_EMAIL",
        payload:{
          email
        }
      })
    }
  }
}

export default connect(mapStateToProps, mapDispathToProps)(PhotoUpload)
