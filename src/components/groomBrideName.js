import React from 'react'
import {connect} from 'react-redux'
import {Input,Row} from 'react-materialize'
import GroomAndBride from '../assets/ui/Bride and Groom.svg'
import ProcessStepper from './processStepper';


class GroomBrideName extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      groom_name:this.props.project.groom_name,
      bride_name:this.props.project.bride_name
    }
  }

  handleChange = (event) => {
    this.setState({
      [event.target.id]:event.target.value
    });
  }

  handleNext = (event) => {
    if(this.state.groom_name&&this.state.bride_name)
    {
      this.props.addGroomBrideName(this.state);
      this.props.history.push('/date');
    }else{
      alert("Please fill in all the fields for the next step");
    }
  }

  render () {
    return(
      <div>
        <ProcessStepper pathname={1}/>
        <form className="row container">
          <div className="row">
          <div className="col s12 m6" id="groomBrideIconContainer">
            <h5 className="center ">Enter Your Details</h5>
            <div id="card" className="col s3">
                <img id="groomAndBride" src={GroomAndBride} alt="groomAndBride"/>
            </div>
          </div>
          <div className="col s12 m6" id="groomBrideInputContainer">
          <Row>
              <Input s={12} id="groom_name" labelClassName="" label="Groom Name" type="text"value={this.state.groom_name} onChange={this.handleChange}/>
              <Input s={12} id="bride_name" labelClassName="" label="Bride Name" type="text" value={this.state.bride_name} onChange={this.handleChange}/>
          </Row>
          </div>
          <div onClick={this.handleNext} id="nextBtnNew">
            <b>Next :</b> Choose Date & Location
          </div>
          </div>
        </form>
      </div>
    )
  }
}

const mapStateToProps = (state) =>{
  return {
    project:state.project
  }
}

const mapDispathToProps = (dispatch) => {
  return {
    addGroomBrideName: (name) => {dispatch({type:"ADD_GROOM_BRIDE_NAME", payload:name})}
  }
}


export default connect(mapStateToProps,mapDispathToProps)(GroomBrideName)
