import React from 'react'
import axios from 'axios';
import { getData, deleteAll, deleteAllToCreateNewProject } from '../scripts/uri';
import {connect} from 'react-redux'

class OrderHistory extends React.Component {
    constructor(props) {
    super(props);
    this.state = {
        data:{}
    }
    }
// 
componentDidMount(){
    axios.get(getData+this.props.auth.user.id).then(res=>{
        console.log(res.data)
        this.setState({data:res.data})
    })
}

createNewProject=(type,value)=>{
    let body = {
      invitation:this.props.project,
      photo:this.props.photo,
      type:type
    }
    axios.post(deleteAllToCreateNewProject,body).then(res=>{
      if(type==="invitation"){
        this.props.clearInvitation();
        this.props.editInvitation(value)
        this.props.invitationRoute()
      }else if(type==="photo")
      {
        this.props.clearPhoto();
        this.props.editPhoto(value)
        this.props.photoRoute()
      }
    })
  }


handleEdit=(invitation)=>{
    // if(window.confirm("Editing will remove unpublished data")===true)
    // {
        let invitations = this.state.data.filter(invitation=>invitation.metadata.productCategory==="invitation")
        if(this.props.project.vuforia)
        {
            let checkInvitation = invitations.filter(invitation=>invitation.metadata.vuforia.target_record.target_id===this.props.project.vuforia.target_record.target_id)
            if(checkInvitation.length>0)
            {
            this.props.clearInvitation();
            this.props.editInvitation(invitation)
            this.props.invitationRoute()
            }else{
            this.createNewProject("invitation",invitation);
            }
        }else{
            this.props.clearInvitation();
            this.props.editInvitation(invitation)
            this.props.invitationRoute()
        }
    // }
}

handleEditPhoto=(photo)=>{
    // if(window.confirm("Editing will remove unpublished data")===true)
    // {
        let photos = this.state.data.filter(photo=>photo.metadata.productCategory==="photo")
        if(this.props.photo.photos)
        {
            if(this.props.photo.photos[0].vuforia)
            {
                let checkPhoto = photos.filter(photo=>photo.metadata.photos[0].vuforia.target_record.target_id===this.props.photo.photos[0].vuforia.target_record.target_id)
                if(checkPhoto.length>0)
                {
                    this.props.clearPhoto();
                    this.props.editPhoto(photo)
                    this.props.photoRoute()
                }else{
                    this.createNewProject("photo",photo);
                }
            }else{
                this.props.clearPhoto();
                this.props.editPhoto(photo)
                this.props.photoRoute()
            }
        }else{
            this.props.clearPhoto();
            this.props.editPhoto(photo)
            this.props.photoRoute()
        }
    // }
}

handleDeleteInvitation=(value)=>{
    if(window.confirm("Are you sure to remove this target?")===true)
    {
        axios.post(deleteAll,value).then(res=>{
            this.setState({data:res.data.data})
            if(value.target_id===this.props.project.target_id)
            {
                this.props.clearInvitation();
            }
            alert("Deleted Successfully")
        })
    }
}

handleDeletePhoto=(value)=>{
    if(window.confirm("Are you sure to remove this target?")===true)
    {
        axios.post(deleteAll,value).then(res=>{
            this.setState({data:res.data.data})
            if(value.target_id===this.props.project.target_id)
            {
                this.props.clearPhoto();
            }
            alert("Deleted Successfully")
        })
    }
}

  render () {
    return(
      <div className="padding20">
        <div className="h3 bold"> Order History</div>
        <div className="orderContainer">
            {this.state.data?
                Object.values(this.state.data).map((value,index)=>
                value.metadata.productCategory==="invitation"?
                    <div key={index} className="col s12 m5 orderWrap">
                    <div className="col s12 orderImageDiv"><img className="orderImage" src={value.metadata.invitationImage} alt="order" width="100%"/></div>
                    <div className="col s12 orderContent">
                        <div>
                            <div className="orderTitle">Invitation</div>
                            <div className="orderStatus">{value.metadata.payment.payment?"Paid":"Trail"}</div>
                        </div>
                        <div className="orderAction">
                            <div to="name" className="edit_from_history" onClick={()=>this.handleEdit(value.metadata)}>Preview</div>
                            <div className="delete_from_history" onClick={()=>this.handleDeleteInvitation(value)}> Delete</div>
                        </div>
                    </div>
                    </div>:
                value.metadata.productCategory==="photo"?
                <div key={index} className="col s12 m5 orderWrap">
                    <div className="col s12 orderImageDiv"><img className="orderImage" src={value.metadata.photos[0].invitationImage} alt="order" width="100%"/></div>
                    <div className="col s12 orderContent">
                        <div>
                            <div className="photoOrderTitleDiv">
                                <div className="orderTitle">Photo -</div>
                                <div className="orderStatus">{value.metadata.photos.length}</div>
                            </div>
                            <div className="orderStatus">{value.metadata.payment.payment?"Paid":"Trail"}</div>
                        </div>
                        <div className="orderAction">
                            <div className="edit_from_history" onClick={()=>this.handleEditPhoto(value.metadata)}>Preview </div>
                            <div className="delete_from_history" onClick={()=>this.handleDeletePhoto(value)}>Delete</div>
                        </div>
                    </div>

                    </div>:null
                ):null}
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
    return {
        project: state.project,
        auth:state.auth,
        photo:state.photo
    }
  }

  const mapDispathToProps = (dispatch) => {
    return {
      editInvitation: (invitation) => {
        dispatch({
          type: "EDIT_INVITATION",
          payload: invitation
        })
      },
      editPhoto: (photo) => {
        dispatch({
          type: "EDIT_PHOTO",
          payload: photo
        })
      },
      clearInvitation: () => {
        dispatch({
          type: "CLEAR_INVITATION"
        })
      },
      clearPhoto: () => {
        dispatch({
          type: "CLEAR_PHOTO"
        })
      }
    }
}

export default connect(mapStateToProps, mapDispathToProps)(OrderHistory)