import React from 'react'
import {connect} from 'react-redux'
import Tick from '../assets/ui/tick.svg'
import ProcessStepper from './processStepper';
import AppStore from '../assets/images/appstore.png'
import PlayStore from '../assets/images/playstore.png'

class Preview extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      upload: "",
      post: "",
      progress: 0,
      imgWidth: 100,
      range:50
    }
  }

  handleNext = (event) => {
    event.preventDefault();
    // this.props.addInvitation(this.state.invitationImageURL, this.state.invitationVideoURL);
    this.props.history.push('/login');
  }

  render() {
    if(!this.props.project.published){
      this.props.history.push('/product')
    }

    return (<div>
        <ProcessStepper pathname={3}/>
        <div className="previewContainer row container">
        <div className="col s12 m6">
          <div>
            <h4 id="previewHeading">Preview</h4>
          </div>
          <div id="Preview">
            <div id="previewTick">
              <img src={Tick} alt="Next" width="15px"/>
            </div>
            <div>Download Augmo App</div>
          </div>
          <div id="Preview">
            <div id="previewTick">
              <img src={Tick} alt="Next" width="15px"/>
            </div>
            <div>Aim and Focus the Invitation</div>
          </div>
          <div id="Preview">
            <div id="previewTick">
                <img src={Tick} alt="Next" width="15px"/>
            </div>
            <div>See the Invitation comes to life</div>
          </div>
          <div className="storeIconWrap">
            <a  target="_blank" rel="noopener noreferrer" href="https://play.google.com/store/apps/details?id=com.BrownButton.Augmo" className="storeIcon">
                <img src={PlayStore} alt="playstore" className="store" width="150px"/>
            </a>
            <a target="_blank" rel="noopener noreferrer" href="https://itunes.apple.com/us/app/augmo/id1261258006?mt=8" className="storeIcon">
                <img src={AppStore} alt="playstore" className="store" width="150px"/>
            </a>
          </div>
          </div>
          <div className="col s12 m6 previewImage">
            <div className="imageEditor">
              <img id="invitationImage" className="previewImageWrap" src={this.props.project.invitationImage} alt=""/>
            </div>
          </div>
          {
            this.props.project.payment.payment?null:
          <div onClick={()=>{this.props.history.push("/payment")}} id="publishBtn">
              Payment
          </div>
          }
          <div onClick={()=>{this.props.published();this.props.history.push("/name")}} id="publishBtn">
              Edit
          </div>
        </div>

    </div>)
  }
}

const mapStateToProps = (state) => {
  return {
    project: state.project,
    photo:state.photo
  }
}

const mapDispathToProps = (dispatch) => {
  return {
    published: () => {dispatch({type:"EDIT_PUBLISHED_INVITATION"})}
  }
}


export default connect(mapStateToProps, mapDispathToProps)(Preview)
