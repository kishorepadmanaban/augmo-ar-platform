import React, { Component } from 'react';
import {BrowserRouter, Route} from 'react-router-dom'
import './App.css';
// import Background from './assets/ui/bg.svg'

import {connect} from 'react-redux'
import {projectActions} from './store/actions/projectActions'
import GroomBrideName from './components/groomBrideName'
import DateLocation from './components/dateLocation'
import InvitationUpload from './components/invitationImage'
import InvitationVideoUpload from './components/invitationVideo'
import GalleryImageUpload from './components/galleryImages'
import Preview from './components/preview'
import PreviewPhoto from './components/previewPhoto'
import Payment from './components/payment'
import PaymentPhoto from './components/paymentPhoto'
import ProductCategory from './components/productCategory'
import EventCategory from './components/eventCategory'
import InvitationPrice from './components/priceInvitation'
import Login from './components/login'

// import GalleryVideoUpload from './components/galleryVideo'
import Template from './components/template'
import Test from './components/test'
import NavBar from './components/navBar'
import firebase from 'firebase/app';
import 'firebase/auth';
import MyOrders from './components/myOrders';
import PhotoPrice from './components/photoPrice';
import PhotoEditor from './components/photoComponent';
import DemoPayment from './demo/demoPayment';

// Configure Firebase.
const config = {
  apiKey:"AIzaSyBQSbXn-ck46DMZLxF6Ai38cKF7qdO4I_Q",
  authDomain:"augmo-platform.firebaseapp.com"
  // ...
};
firebase.initializeApp(config);

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {

    }
  }


  render() {
    // console.log(this.props.location.pathname)
    return (
      <div className="row App">
          <div>
          <BrowserRouter>
          <div>
            <Route path="/" component={NavBar} />
            <Route exact path="/" component={Login}/>
            <Route path="/name" component={GroomBrideName}/>
            <Route path="/product" component={ProductCategory} />
            <Route path="/event" component={EventCategory} />
            <Route path="/price" component={InvitationPrice} />
            <Route path="/photo_price" component={PhotoPrice} />
            <Route path="/photo_editor" component={PhotoEditor} />
            <Route path="/date" component={DateLocation} />
            <Route path="/invitation" component={InvitationUpload} />
            <Route path="/invitationvideo" component={InvitationVideoUpload} />
            <Route path="/gallery" component={GalleryImageUpload} />
            <Route path="/template" component={Template} />
            <Route path="/preview" component={Preview} />
            <Route path="/preview_photo" component={PreviewPhoto} />
            <Route path="/payment" component={Payment} />
            <Route path="/payment_photo" component={PaymentPhoto} />
            <Route path="/test" component={Test} />
            <Route path="/my_orders" component={MyOrders} />
            <Route path="/personal_info" component={MyOrders} />
            <Route path="/manage_address" component={MyOrders} />
            <Route path="/order_history" component={MyOrders} />
            <Route path="/buy_info_card" component={MyOrders} />
            <Route path="/razorpay" component={DemoPayment} />
          </div>
        </BrowserRouter>
      </div>
    </div>
  );
  }
}

const mapStateToProps = (state) =>{
  return {
    projects:state.project
  }
}

const mapDispathToProps = (dispatch) => {
  return {
    deletePosts: (project) => {dispatch(projectActions(project))}
  }
}

export default connect(mapStateToProps,mapDispathToProps)(App);
