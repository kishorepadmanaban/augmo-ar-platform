import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import {BrowserRouter, Route} from 'react-router-dom'
import App from './App';
import * as serviceWorker from './serviceWorker';
import {createStore, /* applyMiddleware */} from 'redux'
import {Provider} from 'react-redux'
// import thunk from 'redux-thunk'
import rootReducer from './store/reducers/rootReducer'

function saveToLocalStorage(state) {
  try {
    const serializedState = JSON.stringify(state)
    localStorage.setItem('state', serializedState)
  } catch(e) {
    console.log(e)
  }
}

function loadFromLocalStorage() {
  try {
    const serializedState = localStorage.getItem('state')
    if (serializedState === null) return undefined
    return JSON.parse(serializedState)
  } catch(e) {
    console.log(e)
    return undefined
  }
}

class Index extends React.Component {
  render(){
    return(
    <BrowserRouter>
        <Route path="/" component={App} />
    </BrowserRouter>
  )}
}


const persistedState = loadFromLocalStorage()

const store = createStore(rootReducer, persistedState, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()/* , applyMiddleware(thunk) */);

ReactDOM.render(<Provider store={store}><Index /></Provider>, document.getElementById('root'));

store.subscribe(() => saveToLocalStorage(store.getState()))
// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
