import React from 'react'
import {connect} from 'react-redux'
const Razorpay = window.Razorpay

class DemoPayment extends React.Component {
  constructor(props) {
    super(props);
    this.state = {

    }
  }

  componentDidMount(){

    var options = {
        "key": "rzp_test_gvCgxv2Z9GOOe0",
        "amount": "2000", // 2000 paise = INR 20
        "name": "Merchant Name",
        "description": "Purchase Description",
        "image": "/your_logo.png",
        "handler": function (response){
            alert(response.razorpay_payment_id);
        },
        "prefill": {
            "name": "Gaurav Kumar",
            "email": "test@test.com"
        },
        "notes": {
            "address": "Hello World"
        },
        "theme": {
            "color": "#F37254"
        }
    };
    var rzp1 = new Razorpay(options);
    
    document.getElementById('rzp-button1').onclick = function(e){
        rzp1.open();
        e.preventDefault();
    }
  }

  render () {
    return(
    <div>
        <button id="rzp-button1">Pay</button>
    </div>

    )
  }
}


const mapStateToProps = (state) => {
  return {project: state.project}
}

const mapDispathToProps = (dispatch) => {
  return {
    addEventCategory: (eventCategory) => {dispatch({type:"ADD_EVENT_CATEGORY", payload:{eventCategory}})}
  }
}


export default connect(mapStateToProps,mapDispathToProps)(DemoPayment)
