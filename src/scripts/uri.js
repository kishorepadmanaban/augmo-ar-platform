export const domain = "http://159.65.146.12/invitation"
export const server = "http://159.65.146.12:3030"
export const image  = "http://159.65.146.12/invitation/assets/images"
export const icon   = "http://159.65.146.12/invitation/assets/icons"


//URI
export const postTarget = "http://159.65.146.12/invitation/vuforia/SampleSelector.php?selelct=PostNewTarget"
export const galleryImages = "http://159.65.146.12:3030/api/galleryimages"
export const removeGalleryImage = "http://159.65.146.12:3030/api/deletegalleryimage/"
export const removeInvitationVideo = "http://159.65.146.12:3030/api/deleteinvitationvideo/"
export const galleryVideo  = "http://159.65.146.12:3030/api/galleryvideo"
export const addInvitationImage = "http://159.65.146.12:3030/api/invitationimage"
export const removeInvitationImage = "http://159.65.146.12:3030/api/deleteinvitationimage/"
export const getTarget = "http://159.65.146.12:3030/api/gettarget/"
export const sendMetadata = "http://159.65.146.12:3030/api/sendmetadata"
export const sendMetadataPhoto = "http://159.65.146.12:3030/api/send_metadata_photo"
export const addPhotoImage = "http://159.65.146.12:3030/api/invitationimage"
export const addPhotoVideo = "http://159.65.146.12:3030/api/invitationvideo"
export const removePhotoImage = "http://159.65.146.12:3030/api/deleteinvitationimage/"
export const removePhotoVideo = "http://159.65.146.12:3030/api/deleteinvitationvideo/"
export const sendPaymentDetails = "http://159.65.146.12:3030/api/send_payment_details"
export const sendPaymentDetailsPhoto = "http://159.65.146.12:3030/api/send_payment_details_photo"


export const addInvitationVideo = "http://159.65.146.12:3030/api/invitationvideo"
export const sendOtp = "http://159.65.146.12:3030/api/sendotp"

export const sendData = "http://159.65.146.12:3030/api/invitation"
export const personalDetails = "http://159.65.146.12:3030/api/personal_details"
export const getData = "http://159.65.146.12:3030/api/invitation/"
export const deleteData = "http://159.65.146.12:3030/api/invitation/"
export const manageAddress = "http://159.65.146.12:3030/api/manage_address/"
export const buyInfoCard = "http://159.65.146.12:3030/api/buy_info_card/"

export const signup = "http://159.65.146.12:3030/api/signup/"
export const login = "http://159.65.146.12:3030/api/login/"
export const forgetPassword = "http://159.65.146.12:3030/api/forget_password/"
export const resetPassword = "http://159.65.146.12:3030/api/reset_password_from_mail/"

export const deleteAll = "http://159.65.146.12:3030/api/delete_all/"
export const deleteAllToLogout = "http://159.65.146.12:3030/api/delete_all_to_logout/"
export const deleteAllToCreateNewProject = "http://159.65.146.12:3030/api/delete_all_to_create_new_project/"
export const deleteTargetFromDatabase = "http://159.65.146.12:3030/api/delete_target_from_database/"






//sales1
export const salesserver = "http://159.65.146.12:5000"


// module.exports = {domain:domain,server:server,icon:icon,image:image,salesserver:salesserver}
